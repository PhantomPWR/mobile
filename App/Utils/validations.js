import * as yup from 'yup';

export const setupValidationSchema = yup.object().shape({
  appName: yup.string().label('App Name / ID').required('Required'),
  apiKey: yup.string().label('API Key').required('Required'),
});

export const loginValidationSchema = yup.object().shape({
  email: yup.string().label('Email').required('Required'),
  password: yup.string().label('Password').required('Required'),
});

export const contactValidationSchema = yup.object().shape({
  firstName: yup.string().label('First Name').required('Required'),
  lastName: yup.string().label('Last Name'),
  email: yup
    .string()
    .label('Email')
    .email('Enter a valid email')
    .required('Required'),
  // allowMarketing: yup.boolean()
  //   .label("Allow Marketing"),
  company: yup.string().label('Company'),
  phoneNumbers: yup.array().label('Phone Numbers'),
});

export const searchContactValidationSchema = yup.object().shape({
  searchType: yup.string().label('Search Type').required('Required'),
  searchTerm: yup.string().label('Search Term').required('Required'),
});

export const createNoteValidationSchema = yup.object().shape({
  type: yup.string().label('Type'),
  title: yup.string().label('Title').required('Required'),
  note: yup.string().label('Note'),
  tags: yup.array().label('Tags'),
});

export const createTaskValidationSchema = yup.object().shape({
  userEmail: yup.string().label('Assign To').required('Required'),
  title: yup.string().label('Title').required('Required'),
  note: yup.string().label('Note'),
  tags: yup.array().label('Tags'),
  actionDate: yup.string().label('Action Date').required('Required'),
  priority: yup.number().label('Priority'),
});
