export const convertNormalizedToArray = (data) => {
  const typesArr = [];

  for (const key in data) {
    const item = data[key];

    typesArr.push({...item, id: key, name: item.name});
  }

  return typesArr;
};
