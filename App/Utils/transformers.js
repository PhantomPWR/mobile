import {merge} from 'lodash';

export const mergeValues = (val1, val2) => {
  let newValue = val1;

  if (Array.isArray(val1)) {
    newValue = merge(val1, val2);
  } else if (typeof val1 === 'object') {
    newValue = {...val1, ...val2};
  } else {
    newValue = val2;
  }

  return newValue;
};
