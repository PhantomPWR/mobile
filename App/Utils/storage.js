import AsyncStorage from '@react-native-community/async-storage';

export const setItem = async (storageKey, value) => {
  const serializedValue =
    typeof value === 'object' ? JSON.stringify(value) : value;
  try {
    await AsyncStorage.setItem(storageKey, serializedValue);
  } catch (err) {
    console.tron.log('Error on setItem', storageKey, value, err);
  }
};

export const getItem = async (storageKey) => {
  let deserializedValue = null;
  let value = null;

  try {
    value = await AsyncStorage.getItem(storageKey);
    deserializedValue = JSON.parse(value);
  } catch (err) {
    deserializedValue = value;
    console.tron.log(
      !value ? 'Error on getItem' : 'getItem value is of type String',
      storageKey,
      err,
      value,
    );
  }

  return deserializedValue;
};
