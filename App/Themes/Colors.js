const PRIMARY = '#663399';
const SECONDARY = `${PRIMARY}aa`;
const TINT1 = `${PRIMARY}88`;
const TINT2 = `${PRIMARY}66`;
const TINT3 = `${PRIMARY}44`;

const colors = {
  primary: '#663399',
  lightPrimary: '#754e9c',
  secondary: '#663399aa',
  tint1: '#66339988',
  tint2: '#66339966',
  tint3: '#66339944',
  text: '#353b48',
  mutedText: '#7f8fa6',
  yellow: '#FF8C00',
  lightYellow: '#ff9c24',
  white: '#FFFFFF',
  silver: '#F7F7F7',
  background: '#F4F5F9',
  border: '#dcdde1',
  darkGray: '#C4C4C6',
  darkerGray: '#AAAAAA',
  darkestGray: '#888888',
  gray: '#353b48',
  lightGray: '#6464625A',
  lighterGray: '#DDDDDD',
  lightestGray: '#f5f6fa',
  error: '#FF4444',
  black: '#333333',
  success: '#34C75A',
  small: '#718093',
  transparent: 'transparent',
};

export const setPrimaryColor = (color) => {
  colors.primary = color;
  colors.secondary = `${color}aa`;
  colors.tint1 = `${color}88`;
  colors.tint2 = `${color}66`;
  colors.tint3 = `${color}44`;
};

export const resetColors = () => {
  colors.primary = PRIMARY;
  colors.secondary = SECONDARY;
  colors.tint1 = TINT1;
  colors.tint2 = TINT2;
  colors.tint3 = TINT3;
};

export default colors;
