import Colors from './Colors';
import Metrics from './Metrics';
import {moderateScale} from 'react-native-size-matters/extend';

const fontSizes = {
  xxxsmall: 10,
  xxsmall: 11,
  xsmall: 12,
  small: 13,
  medium: 14,
  slarge: 15,
  large: 16,
  xlarge: 17,
  xxlarge: 18,
  xxxlarge: 20,
  xxxxlarge: 22,
};

const getFontByWeight = (weight) => {
  switch (weight) {
    case 'light':
      return 'FiraSans-Light';
    case 'medium':
      return 'FiraSans-Medium';
    case 'semibold':
      return 'FiraSans-SemiBold';
    case 'bold':
      return 'FiraSans-Bold';
    default:
      return 'FiraSans-Regular';
  }
};

const ApplicationStyles = {
  rootContainer: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  header: {
    zIndex: 5000,
    // backgroundColor: Colors.primary
  },
  panelHeader: {
    height: moderateScale(44, 0.3),
  },
  formRow: {
    height: 44,
    paddingLeft: Metrics.spaceHorizontal,
  },
  button: {
    height: 48,
  },
  floatingButtonLength: moderateScale(50, 0.1),
  getFontByWeight,
  fontSizes,
};

export default ApplicationStyles;
