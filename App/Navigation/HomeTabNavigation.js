import React from 'react';
import DashboardScreen from '../Containers/HomeScreen/DashboardScreen';
import NotificationsScreen from '../Containers/HomeScreen/NotificationsScreen';
import QueriesScreen from '../Containers/HomeScreen/QueriesScreen';
import {Colors, ApplicationStyles} from '../Themes';

import {moderateScale} from 'react-native-size-matters/extend';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();

const HomeTabNavigation = () => {
  const insets = useSafeAreaInsets();
  const bottomInset = insets.bottom;

  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      tabBarOptions={{
        tabStyle: {
          height: moderateScale(50, 0.3),
        },
        indicatorStyle: {
          backgroundColor: Colors.primary,
        },
        activeTintColor: Colors.primary,
        inactiveTintColor: Colors.darkerGray,
        labelStyle: {
          fontFamily: ApplicationStyles.getFontByWeight('medium'),
          fontSize: moderateScale(12, 0.3),
        },
      }}>
      <Tab.Screen component={DashboardScreen} name="Dashboard" />
      <Tab.Screen component={NotificationsScreen} name="Notifications" />
      <Tab.Screen component={QueriesScreen} name="Queries" />
    </Tab.Navigator>
  );
};

export default HomeTabNavigation;
