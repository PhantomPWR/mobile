import React, {useCallback} from 'react';
import Details from '../Containers/DataObjectScreens/DataObjectItemsScreen/Details';
import Relationships from '../Containers/DataObjectScreens/DataObjectItemsScreen/Relationships';
import Notes from '../Containers/DataObjectScreens/DataObjectItemsScreen/Notes';
import Attachments from '../Containers/DataObjectScreens/DataObjectItemsScreen/Attachments';
import {Colors, ApplicationStyles} from '../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();

const DataObjectTabNavigation = ({items, connectedContacts, attachments}) => {
  const renderDetails = useCallback(
    (props) => {
      return <Details {...props} items={items} />;
    },
    [items],
  );

  const renderRelationships = useCallback(
    (props) => {
      return <Relationships {...props} connectedContacts={connectedContacts} />;
    },
    [connectedContacts],
  );

  const renderNotes = useCallback((props) => {
    return <Notes {...props} />;
  }, []);

  const renderAttachments = useCallback(
    (props) => {
      return <Attachments {...props} attachments={attachments} />;
    },
    [attachments],
  );

  return (
    <Tab.Navigator
      initialRouteName="Details"
      sceneContainerStyle={{
        backgroundColor: Colors.white,
      }}
      tabBarOptions={{
        // style: {
        //   marginBottom: moderateScale(8, 0.3),
        // },
        tabStyle: {
          height: moderateScale(46, 0.3),
        },
        indicatorStyle: {
          backgroundColor: Colors.primary,
        },
        activeTintColor: Colors.primary,
        inactiveTintColor: Colors.lightGray,
        labelStyle: {
          fontFamily: ApplicationStyles.getFontByWeight('medium'),
          fontSize: moderateScale(12, 0.3),
          margin: 0,
        },
      }}>
      <Tab.Screen
        component={renderDetails}
        name="Details"
        options={{title: 'Details'}}
      />
      <Tab.Screen
        component={renderRelationships}
        name="Relationships"
        options={{title: "R'ships"}}
      />
      <Tab.Screen
        component={renderNotes}
        name="Notes"
        options={{title: 'Notes'}}
      />
      <Tab.Screen
        component={renderAttachments}
        name="Files"
        options={{title: 'Files'}}
      />
    </Tab.Navigator>
  );
};

export default DataObjectTabNavigation;
