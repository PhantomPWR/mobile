import React from 'react';
import {NoteScreen, TaskScreen} from '../Containers/NoteTaskScreens';
import {DataObjectTypesScreen} from '../Containers/DataObjectScreens';

import {Colors, ApplicationStyles} from '../Themes';

import {moderateScale} from 'react-native-size-matters/extend';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();

const ContactTabNavigation = () => {
  return (
    <Tab.Navigator
      initialRouteName="Notes"
      tabBarOptions={{
        tabStyle: {
          height: moderateScale(50, 0.3),
        },
        indicatorStyle: {
          backgroundColor: Colors.primary,
        },
        activeTintColor: Colors.primary,
        inactiveTintColor: Colors.lightGray,
        labelStyle: {
          fontFamily: ApplicationStyles.getFontByWeight('medium'),
          fontSize: moderateScale(12, 0.3),
        },
      }}>
      <Tab.Screen
        component={NoteScreen}
        name="Notes"
        options={{title: 'Notes'}}
      />
      <Tab.Screen
        component={TaskScreen}
        name="Tasks"
        options={{title: 'Tasks'}}
      />
      <Tab.Screen
        component={DataObjectTypesScreen}
        name="DataObjects"
        options={{title: 'Data Types'}}
      />
    </Tab.Navigator>
  );
};

export default ContactTabNavigation;
