import React from 'react';
import {Animated} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Setup from '../Containers/LoginScreen/Setup';
import Login from '../Containers/LoginScreen/Login';
import {Colors} from '../Themes';

const Stack = createStackNavigator();

const forSlide = ({current, next, inverted, layouts: {screen}}) => {
  const progress = Animated.add(
    current.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
    next
      ? next.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: 'clamp',
        })
      : 0,
  );

  return {
    cardStyle: {
      transform: [
        {
          translateX: Animated.multiply(
            progress.interpolate({
              inputRange: [0, 1, 2],
              outputRange: [
                screen.width, // Focused, but offscreen in the beginning
                0, // Fully focused
                -screen.width, // Fully unfocused
              ],
              extrapolate: 'clamp',
            }),
            inverted,
          ),
        },
      ],
    },
  };
};

const AuthNavigation = () => {
  return (
    <Stack.Navigator
      initialRouteName="Setup"
      screenOptions={() => ({
        headerShown: false,
        cardStyle: {
          backgroundColor: Colors.background,
        },
        cardStyleInterpolator: forSlide,
      })}>
      <Stack.Screen component={Setup} name="Setup" />
      <Stack.Screen component={Login} name="Login" />
    </Stack.Navigator>
  );
};

export default AuthNavigation;
