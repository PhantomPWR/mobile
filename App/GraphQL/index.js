import {client} from '..';
import {setupDetailsVar} from '../Cache';
import Const from '../Constants';

const handleCallQuery = (graphqlQuery, variables, mutationKey, fetchPolicy) => {
  const payload = mutationKey ? {[mutationKey]: variables} : variables;
  const policy = mutationKey ? Const.FETCH_POLICIES.NO_CACHE : fetchPolicy;
  const graphQlParams = {
    [mutationKey ? 'mutation' : 'query']: graphqlQuery,
    variables: payload,
    fetchPolicy: policy,
  };

  console.tron.log('handleCallQuery', mutationKey, graphQlParams, policy);

  return client[mutationKey ? 'mutate' : 'query'](graphQlParams);
};

export const callGraphqlQuery = async (
  graphqlQuery,
  variables,
  mutationKey,
  fetchPolicy,
) => {
  let {appName, apiKey, ...payload} = variables;

  if (!appName || !apiKey) {
    const {
      appName: cachedAppName,
      apiKey: cachedApiKey,
    } = await setupDetailsVar();
    appName = cachedAppName;
    apiKey = cachedApiKey;
  }

  const response = await handleCallQuery(
    graphqlQuery,
    {appName, apiKey, ...payload},
    mutationKey,
    fetchPolicy,
  );

  return response;
};

export const readGraphqlCache = (graphqlQuery, payload) => {
  let {appName, apiKey} = setupDetailsVar(null, null, {directCache: true});

  const variables = {
    ...(apiKey ? {appName, apiKey} : {}),
    ...payload,
  };

  const response = client.cache.readQuery({
    query: graphqlQuery,
    variables,
  });

  return response;
};
