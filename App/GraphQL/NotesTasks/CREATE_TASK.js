import gql from 'graphql-tag';

export default gql`
  mutation createTask($createTaskInput: CreateTaskInput!) {
    createTask(input: $createTaskInput) {
      id
    }
  }
`;
