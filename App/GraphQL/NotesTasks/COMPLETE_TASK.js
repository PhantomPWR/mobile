import gql from 'graphql-tag';

export default gql`
  mutation completeTask($completeTaskInput: CompleteTaskInput!) {
    completeTask(input: $completeTaskInput) {
      id
    }
  }
`;
