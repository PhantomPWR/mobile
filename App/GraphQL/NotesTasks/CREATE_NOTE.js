import gql from 'graphql-tag';

export default gql`
  mutation createNote($createNoteInput: CreateNoteInput!) {
    createNote(input: $createNoteInput) {
      id
    }
  }
`;
