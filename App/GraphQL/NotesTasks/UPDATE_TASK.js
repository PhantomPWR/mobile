import gql from 'graphql-tag';

export default gql`
  mutation updateTask($updateTaskInput: UpdateTaskInput!) {
    updateTask(input: $updateTaskInput) {
      id
    }
  }
`;
