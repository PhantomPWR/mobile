import gql from 'graphql-tag';

export default gql`
  query listNotes(
    $appName: String!
    $apiKey: String!
    $contactId: ID!
    $page: Int
    $limit: Int
    $filter: String
    $filterBy: String
  ) {
    listNotes(
      appName: $appName
      apiKey: $apiKey
      contactId: $contactId
      page: $page
      limit: $limit
      filter: $filter
      filterBy: $filterBy
    ) {
      items {
        id
        accepted
        userId
        creationDate
        contactId
        completionDate
        lastUpdated
        lastUpdatedBy
        endDate
        type
        actionDate
        title
        noteType
        note
        tags
      }
      count
      total
    }
  }
`;
