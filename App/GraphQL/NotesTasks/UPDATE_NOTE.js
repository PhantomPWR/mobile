import gql from 'graphql-tag';

export default gql`
  mutation updateNote($updateNoteInput: UpdateNoteInput!) {
    updateNote(input: $updateNoteInput) {
      id
    }
  }
`;
