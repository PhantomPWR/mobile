import gql from 'graphql-tag';

export default gql`
  query listDataObjects($appName: String!, $apiKey: String!) {
    listDataObjects(appName: $appName, apiKey: $apiKey) {
      id
      title
    }
  }
`;
