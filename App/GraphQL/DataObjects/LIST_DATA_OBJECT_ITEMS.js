import gql from 'graphql-tag';

export default gql`
  query listDataObjectItems(
    $appName: String!
    $apiKey: String!
    $email: String
    $groupId: ID!
    $contactId: ID!
  ) {
    listDataObjectItems(
      appName: $appName
      apiKey: $apiKey
      email: $email
      groupId: $groupId
      contactId: $contactId
    ) {
      items {
        id
        data {
          fieldId
          name
          type
          value
          subGroup
          sectionTag
        }
        attachments {
          id
          fileName
          thumbnail
          downloadUrl
        }
        meta {
          editable
          searchable
          multiple_link
        }
        connectedContacts {
          id
          firstName
          lastName
          email
          relationships {
            id
            role
          }
        }
      }
      count
      next
      previous
      fields {
        fieldId
        key
        type
        helpText
        placeholder
        default
        required
        showInTable
        showOrder
      }
    }
  }
`;
