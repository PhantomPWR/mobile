import gql from 'graphql-tag';

export default gql`
  mutation createOrUpdateContact(
    $createOrUpdateContactInput: CreateOrUpdateContactInput!
  ) {
    createOrUpdateContact(input: $createOrUpdateContactInput) {
      id
    }
  }
`;
