import gql from 'graphql-tag';

export default gql`
  query listContacts(
    $appName: String!
    $apiKey: String!
    $q: String
    $page: Int
    $limit: Int
  ) {
    listContacts(
      appName: $appName
      apiKey: $apiKey
      q: $q
      page: $page
      limit: $limit
    ) {
      items {
        id
        email
        firstName
        lastName
        title
        company
        emailAddress2
        emailAddress3
        website
        jobTitle
        phoneNumbers
        streetAddress1
        streetAddress2
        city
        state
        postalCode
        country
        address2Street1
        address2Street2
        city2
        state2
        postalCode2
        country2
      }
      count
    }
  }
`;
