import gql from 'graphql-tag';

export default gql`
  query login(
    $appName: String!
    $apiKey: String!
    $email: String!
    $password: String!
  ) {
    login(
      appName: $appName
      apiKey: $apiKey
      email: $email
      password: $password
    ) {
      sessionId
      firstName
      lastName
    }
  }
`;
