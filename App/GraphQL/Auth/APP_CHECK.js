import gql from 'graphql-tag';

export default gql`
  query appCheck($appName: String!, $apiKey: String!) {
    appCheck(appName: $appName, apiKey: $apiKey) {
      success
    }
  }
`;
