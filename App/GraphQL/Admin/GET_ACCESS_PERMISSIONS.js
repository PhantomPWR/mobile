import gql from 'graphql-tag';

export default gql`
  query getAccessPermissions(
    $appName: String!
    $apiKey: String!
    $email: String!
  ) {
    getAccessPermissions(appName: $appName, apiKey: $apiKey, email: $email) {
      level
      sections {
        id
        access {
          name
          permission
        }
      }
    }
  }
`;
