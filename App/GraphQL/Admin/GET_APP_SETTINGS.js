import gql from 'graphql-tag';

export default gql`
  query getAppSettings($appName: String!, $apiKey: String!) {
    getAppSettings(appName: $appName, apiKey: $apiKey) {
      uiColour
    }
  }
`;
