import React, {useEffect} from 'react';
import {View, StatusBar} from 'react-native';
import {ApplicationStyles} from '../Themes';
import AppNavigation from '../Navigation/AppNavigation';
import {NavigationContainer} from '@react-navigation/native';
import RNBootSplash from 'react-native-bootsplash';

const RootContainer = () => {
  useEffect(() => {
    setTimeout(() => {
      RNBootSplash.hide({duration: 250});
    }, 500);
  }, []);

  return (
    <View style={ApplicationStyles.rootContainer}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent
      />
      <NavigationContainer>
        <AppNavigation />
      </NavigationContainer>
    </View>
  );
};

export default RootContainer;

// wraps dispatch to create nicer functions to call within our component
// const mapDispatchToProps = dispatch => ({
//   startup: () => dispatch(StartupActions.startup())
// })

// export default connect(
//   null,
//   mapDispatchToProps
// )(RootContainer)
