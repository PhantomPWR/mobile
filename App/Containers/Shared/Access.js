import React, {useEffect} from 'react';
import {Modal, View} from '../../Components/Layout';
import {moderateScale} from 'react-native-size-matters/extend';
import {GET_ACCESS_PERMISSIONS} from '../../GraphQL/Admin';
import {userDetailsVar} from '../../Cache';
import {useQuery, useReactiveVar} from '../../Hooks';
import Const from '../../Constants';
import {readGraphqlCache} from '../../GraphQL';

const Access = (props) => {
  const {type, name, isMutation, children} = props;

  if (hasAccess(type, name, isMutation)) {
    return children;
  }

  return null;
};

export const getAccessPermissions = () => {
  const {email} = userDetailsVar(null, null, {directCache: true});

  const dataAccessPermissions = readGraphqlCache(GET_ACCESS_PERMISSIONS, {
    email,
  });

  return dataAccessPermissions.getAccessPermissions;
};

export const hasAccess = (type, name, isMutation) => {
  const accessPermissions = getAccessPermissions();

  console.tron.log('accessPermissions', accessPermissions);

  if (accessPermissions.level === 'admin') {
    return true;
  }

  let permission;

  switch (type) {
    case 'section': {
      const {sections} = accessPermissions;
      const dataPermission = sections.reduce((value, section) => {
        const access = section.access.find((item) => name === item.name);

        if (access) {
          return access.permission;
        }
      }, '');

      console.tron.log('dataPermission: ', dataPermission);

      permission = dataPermission;
      break;
    }
    case 'contact': {
      const {contacts} = accessPermissions;
      const dataPermission = contacts.reduce((value, contact) => {
        const permission = contact[name];

        if (permission) {
          return permission;
        }
      }, '');

      console.tron.log('dataPermission: ', dataPermission);

      permission = dataPermission;
      break;
    }
  }

  if (
    permission === Const.ACCESS_LEVELS.READ_WRITE ||
    (permission === Const.ACCESS_LEVELS.READ && !isMutation)
  ) {
    return true;
  }

  return null;
};

export default Access;
