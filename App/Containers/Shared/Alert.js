import React, {forwardRef} from 'react';
import {Modal, View} from '../../Components/Layout';
import {moderateScale} from 'react-native-size-matters/extend';

const Alert = forwardRef((props, ref) => {
  const {children, disableBackdropPress, isVisible, style, ...allProps} = props;

  return (
    <Modal
      alignCenter
      disableBackdropPress={disableBackdropPress}
      isVisible={isVisible}
      justifyCenter
      ref={ref}>
      <View
        // alignCenter
        style={{
          backgroundColor: '#F2F2F2',
          borderRadius: 14,
          width: moderateScale(268, 0.3),
          // height: moderateScale(178, 0.1),
          ...style,
        }}
        {...allProps}>
        {children}
      </View>
    </Modal>
  );
});

export default Alert;
