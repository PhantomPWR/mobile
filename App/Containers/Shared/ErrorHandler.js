import {
  setJSExceptionHandler,
  setNativeExceptionHandler,
} from 'react-native-exception-handler';
import DebugConfig from '../../Config/DebugConfig';
import SentryConfig from '../../Config/SentryConfig';

const jsErrorHandler = (e) => {
  console.log('TRIGGERED GLOBAL JS ERROR HANDLER: ', e);
  if (DebugConfig.enableSentry) {
    SentryConfig.captureException(e);
  }
};

const nativeErrorHandler = (errorString) => {
  console.log('TRIGGERED GLOBAL NATIVE ERROR HANDLER: ' + errorString);
  if (DebugConfig.enableSentry) {
    SentryConfig.captureException('NATIVE ERROR: ' + errorString);
  }
};

setJSExceptionHandler(jsErrorHandler);

setNativeExceptionHandler(nativeErrorHandler, true, true);
