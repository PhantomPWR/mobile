import React from 'react';

import {FloatingLabelSelect} from '../../../Components/Forms';

import {moderateScale} from 'react-native-size-matters/extend';

const FormSelect = (props) => {
  const {onSelectPickerItem, defaultValue, ...allProps} = props;

  return (
    <FloatingLabelSelect
      defaultValue={defaultValue}
      inputPaddingTop={moderateScale(8, 0.3)}
      onSelectPickerItem={onSelectPickerItem}
      // containerStyle={{
      //   flex: 1
      // }}
      style={{
        justifyContent: 'space-between',
        // paddingRight: Metrics.spaceHorizontal
      }}
      {...allProps}
    />
  );
};

FormSelect.defaultProps = {
  zIndex: 1000,
  defaultValue: '',
  onSelectPickerItem: () => {},
};

export default FormSelect;
