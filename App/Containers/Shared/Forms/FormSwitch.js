import React, {useRef} from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {View} from '../../../Components/Layout';
import {Text} from '../../../Components/Text';
import {Switch} from '../../../Components/Forms';
import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const SWITCH_HEIGHT = moderateScale(52, 0.3);

const FormSwitch = (props) => {
  const switchRef = useRef();
  const {label, onValueChange, defaultValue, style, ...allProps} = props;

  const onPressRow = () => {
    switchRef.current.toggleSwitch();
  };

  return (
    <TouchableHighlight
      onPress={onPressRow}
      style={[styles.rowItem, style]}
      underlayColor={Colors.silver}
      {...allProps}>
      <>
        <View
          isFlex
          style={{
            paddingRight: moderateScale(24),
          }}>
          <Text slarge>{label}</Text>
        </View>

        <View>
          <Switch
            defaultValue={defaultValue}
            onValueChange={onValueChange}
            ref={switchRef}
          />
        </View>
      </>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  rowItem: {
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: SWITCH_HEIGHT,
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.spaceHorizontal,
  },
});

FormSwitch.defaultProps = {
  onValueChange: () => {},
};

export default FormSwitch;
