import React from 'react';
import {DatePicker} from '../../../Components/Forms';
import {moderateScale} from 'react-native-size-matters/extend';

const FormDatePicker = (props) => {
  return (
    <DatePicker
      inputPaddingTop={moderateScale(8, 0.3)}
      style={
        {
          // paddingHorizontal: Metrics.spaceHorizontal
        }
      }
      {...props}
    />
  );
};

export default FormDatePicker;
