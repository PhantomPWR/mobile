import React from 'react';
import {StyleSheet} from 'react-native';
import {View} from '../../../Components/Layout';
import {IconButton} from '../../../Components/Buttons';
import {Colors, Metrics, ApplicationStyles} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import HeaderLeft from './HeaderLeft';
import HeaderCenter from './HeaderCenter';
import HeaderRight from './HeaderRight';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {useNavigation} from '@react-navigation/native';
import {Heading} from '../../../Components/Text';

const Header = (props) => {
  const navigation = useNavigation();
  const {
    noInset,
    hasBack,
    headerStyle,
    headerTitle,
    headerLeft,
    headerCenter,
    headerRight,
    barStyle,
    onBack,
    ...allProps
  } = props;
  const insets = useSafeAreaInsets();
  const canGoBack = hasBack && navigation.canGoBack();

  const topInset = !noInset ? insets.top : 0;
  const BAR_HEIGHT = moderateScale(44, 0.4);

  return (
    <View
      // row
      justifyEnd
      style={[
        styles.headerContainer,
        {
          paddingTop: topInset,
          // paddingBottom: moderateScale(8, 0.01),
          minHeight: topInset + moderateScale(58, 0.4),
          borderBottomWidth: 1,
          borderBottomColor: Colors.border,
          backgroundColor: Colors.primary,
          ...ApplicationStyles.header,
          ...headerStyle,
        },
      ]}
      {...allProps}>
      <View
        // isFlex
        alignCenter
        justifyBetween
        row
        style={[
          {
            height: BAR_HEIGHT,
          },
          barStyle,
        ]}>
        {headerTitle && (
          <View
            style={
              canGoBack
                ? {
                    position: 'absolute',
                    left: Metrics.spaceHorizontal + moderateScale(44, 0.4),
                    right: Metrics.spaceHorizontal + moderateScale(44, 0.4),
                    alignItems: 'center',
                  }
                : {
                    marginLeft: Metrics.spaceHorizontal,
                  }
            }>
            <Heading
              color="white"
              noMargin
              numberOfLines={1}
              style={canGoBack && {fontSize: moderateScale(20, 0.3)}}>
              {headerTitle}
            </Heading>
          </View>
        )}

        {canGoBack && (
          <IconButton
            icon="LeftChevron"
            iconFill={Colors.white}
            iconSize={moderateScale(20, 0.1)}
            onPress={onBack || navigation.goBack}
          />
        )}

        {headerLeft && <HeaderLeft children={headerLeft} />}

        <HeaderRight children={headerRight} />
      </View>

      {headerCenter && <HeaderCenter children={headerCenter} />}
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: Colors.white,
  },
});

export default Header;
