import React from 'react';
import {View} from '../../../Components/Layout';

const HeaderCenter = (props) => {
  const {title, style, children, ...allProps} = props;

  return (
    <View
      // isFlex
      // justifyEnd
      style={[
        {
          // ...Metrics.absoluteFill,
          // paddingHorizontal: Metrics.spaceHorizontal,
          // backgroundColor: 'red'
        },
        style,
      ]}
      {...allProps}>
      {children}
    </View>
  );
};

export default HeaderCenter;
