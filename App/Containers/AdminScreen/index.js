import React from 'react';
import {ScrollView} from 'react-native';
import {Content, View} from '../../Components/Layout';
import {Button} from '../../Components/Buttons';
import TabContainer from '../Shared/TabContainer';
import {resetColors} from '../../Themes/Colors';
import {moderateScale} from 'react-native-size-matters/extend';
import {sessionIdVar} from '../../Cache';

const AdminScreen = (props) => {
  const handleLogout = () => {
    resetColors();
    sessionIdVar('');
    props.navigation.replace('Login');
  };

  return (
    <TabContainer headerTitle="Admin">
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          minHeight: '100%',
        }}
        keyboardShouldPersistTaps="always">
        <Content>
          <View
            style={{
              position: 'absolute',
              bottom: moderateScale(20, 0.3),
              width: '100%',
              alignSelf: 'center',
            }}>
            <Button
              label="Log out"
              onPress={handleLogout}
              // isLoading={isLoading}
              // icon={
              //   <View style={{
              //     marginLeft: 10
              //   }}>
              //     <SvgIcon fill={Colors.white} name='RightChevron' width={moderateScale(10)} height={moderateScale(10)}/>
              //   </View>
              // }
            />
          </View>
        </Content>
      </ScrollView>
    </TabContainer>
  );
};

export default AdminScreen;
