import React from 'react';
import {ScrollView} from 'react-native';
import {Content} from '../../Components/Layout';
import TabContainer from '../Shared/TabContainer';

const SavedSearchesScreen = (props) => {
  return (
    <TabContainer headerTitle={'Saved Searches'}>
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          minHeight: '100%',
        }}
        keyboardShouldPersistTaps="always">
        <Content />
      </ScrollView>
    </TabContainer>
  );
};

export default SavedSearchesScreen;
