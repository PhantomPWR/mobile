import React, {useState, useRef} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '../../Components/SvgIcon';
import {View, Content} from '../../Components/Layout';
import {Button} from '../../Components/Buttons';
import {Colors} from '../../Themes';
import {ErrorMessage, Label} from '../../Components/Text';
import {moderateScale} from 'react-native-size-matters/extend';
import {FormInput} from '../../Components/Forms';
import {setupValidationSchema} from '../../Utils/validations';
import {Formik} from 'formik';
import LoginAlerts from './LoginAlerts';
import {setupDetailsVar} from '../../Cache';
import {useWindowDimensions, useQuery} from '../../Hooks';

const questionIconSize = moderateScale(16);

const Setup = (props) => {
  const modalRef = useRef();
  // const { loading: loadingInitValues, data } = useQuery(GET_SETUP_DETAILS);
  // const initialValues = data.setupDetails || {}
  const {data: setupDetails} = useQuery(Setup.name, {cacheFn: setupDetailsVar});
  const {windowWidth} = useWindowDimensions();

  // console.tron.log("initial Setup details", setupDetails)

  // useEffect(() => {
  //   setTimeout(() => {
  //     console.tron.log("setup details update")
  //     setupDetailsVar({})
  //   }, 1000)
  // }, [])

  const [alertInfo, setAlertInfo] = useState({type: '', payload: {}});
  const [setupError, setSetupError] = useState('');
  // const dispatch = useDispatch()
  const appNameRef = useRef();
  const apiKeyRef = useRef();

  const showAlertInfo = (type = '', payload = {}) => {
    setAlertInfo({type, payload});
    modalRef.current.show();
  };

  const onConnect = () => {
    props.navigation.navigate('Login');
  };

  const onError = (error) => {
    setSetupError(error);
  };

  return (
    <View isFlex>
      <Formik
        initialValues={setupDetails}
        key={JSON.stringify(setupDetails)}
        onSubmit={(values) => {
          const {appName, apiKey} = values;

          setSetupError('');
          setupDetailsVar(values);

          showAlertInfo('connecting', {appName, apiKey});
        }}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={setupValidationSchema}>
        {({handleChange, values, handleSubmit, errors}) => (
          <Content largeSpace>
            <FormInput
              defaultValue={values.appName}
              error={errors.appName}
              icon="FingerPrint"
              onChangeText={handleChange('appName')}
              onSubmitEditing={() => apiKeyRef.current.focus()}
              placeholder="App Name / ID"
              ref={appNameRef}
              returnKeyType="next"
            />

            <FormInput
              defaultValue={values.apiKey}
              error={errors.apiKey}
              icon="Security"
              onChangeText={handleChange('apiKey')}
              onSubmitEditing={handleSubmit}
              placeholder="API Key"
              ref={apiKeyRef}
              returnKeyType="done"
            />

            <Button
              icon={
                <View
                  style={{
                    marginLeft: 10,
                  }}>
                  <SvgIcon
                    fill={Colors.white}
                    height={moderateScale(10)}
                    name="RightChevron"
                    width={moderateScale(10)}
                  />
                </View>
              }
              label="Connect"
              // isLoading={isLoading}
              onPress={handleSubmit}
            />
            <ErrorMessage center>{setupError}</ErrorMessage>

            <View justifyBetween>
              <TouchableOpacity
                onPress={() => showAlertInfo()}
                style={styles.options}>
                <SvgIcon
                  fill={Colors.secondary}
                  height={questionIconSize}
                  name="Help"
                  width={questionIconSize}
                />
                <Label
                  style={{
                    color: Colors.primary,
                    marginLeft: moderateScale(8),
                  }}>
                  Where are my setup details?
                </Label>
              </TouchableOpacity>
            </View>
          </Content>
        )}
      </Formik>

      {
        <LoginAlerts
          alertInfo={alertInfo}
          onError={onError}
          onSuccess={onConnect}
          ref={modalRef}
        />
      }
    </View>
  );
};

const styles = StyleSheet.create({
  options: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: moderateScale(36, 0.3),
  },
});

export default Setup;
