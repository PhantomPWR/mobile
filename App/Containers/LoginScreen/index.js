import React from 'react';
import {StyleSheet} from 'react-native';

import {View, Container} from '../../Components/Layout';
import {SubHeading} from '../../Components/Text';
import {Image} from '../../Components/Media';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, Metrics, Images} from '../../Themes';
import AuthNavigation from '../../Navigation/AuthNavigation';

const logoSize = moderateScale(104);

const LoginScreen = (props) => {
  return (
    <Container>
      <View alignCenter isFlex justifyEnd>
        <Image height={logoSize} source={Images.logo} width={logoSize} />
        <View>
          <SubHeading style={styles.appName}>Welcome to Macanta</SubHeading>
        </View>
      </View>

      <AuthNavigation />

      {/* <Animated.View
        style={{
          width: windowWidth * 2,
          flex: 1,
          flexDirection: 'row',
          transform: [
            {
              translateX: animatedIsLogin.interpolate({
                inputRange: [0, 1],
                outputRange: [0, -windowWidth],
              }),
            },
          ],
        }}>
        <View isFlex>
          <Setup onConnect={onConnect} />
        </View>
        <View isFlex>
          <Login
            onBack={showLogin.bind(null, false)}
            ref={loginRef}
            setupDetails={setupDetails}
          />
        </View>
      </Animated.View> */}
    </Container>
  );
};

const styles = StyleSheet.create({
  appName: {
    color: Colors.primary,
    marginBottom: Metrics.hp(7.2),
    marginTop: moderateScale(16),
  },
});

export default LoginScreen;
