import React, {forwardRef, useEffect, useRef, useImperativeHandle} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {View} from '../../Components/Layout';
import {Text} from '../../Components/Text';
import {Colors} from '../../Themes';
import Alert from '../../Containers/Shared/Alert';
import {moderateScale} from 'react-native-size-matters/extend';
import {APP_CHECK, LOGIN} from '../../GraphQL/Auth';
import {useQuery} from '../../Hooks';
import Loader from '../../Components/Loader';

const Connecting = (props) => {
  const {onSuccess, setShowAlert, onError, payload} = props;
  const {error, data} = useQuery(APP_CHECK, payload);

  useEffect(() => {
    if (data.appCheck && data.appCheck.success) {
      setTimeout(() => {
        setShowAlert(false);
        onSuccess(payload);
      }, 1000);
    } else if (error) {
      console.tron.log('Connecting ERROR', error);
      setShowAlert(false);
      onError(error);
    }
  }, [data.appCheck, error]);

  return (
    <>
      <View
        isFlex
        style={{
          paddingHorizontal: moderateScale(20),
          paddingVertical: moderateScale(20),
        }}>
        <Text
          center
          style={{
            marginBottom: 2,
          }}
          weight="medium"
          xlarge>
          Connection Progress
        </Text>
        <View
          alignCenter
          justifyCenter
          row
          style={{
            marginTop: moderateScale(35),
            backgroundColor: Colors.white,
            height: moderateScale(40),
          }}>
          <View
            style={{
              position: 'absolute',
              left: moderateScale(24),
            }}>
            <Loader />
          </View>
          <Text color="darkerGray" large>
            Connecting...
          </Text>
        </View>
      </View>
    </>
  );
};

const LoggingIn = (props) => {
  const {onSuccess, setShowAlert, onError, payload} = props;
  const {error, data} = useQuery(LOGIN, payload);

  useEffect(() => {
    if (data.login) {
      setTimeout(() => {
        setShowAlert(false);
        onSuccess(data.login);
      }, 1000);
    } else if (error) {
      console.tron.log('LoggingIn ERROR', error);
      setShowAlert(false);
      onError(error);
    }
  }, [data.login, error]);

  return (
    <>
      <View
        isFlex
        style={{
          paddingHorizontal: moderateScale(20),
          paddingVertical: moderateScale(20),
        }}>
        <Text
          center
          style={{
            marginBottom: 2,
          }}
          weight="medium"
          xlarge>
          Log In Progress
        </Text>
        <View
          alignCenter
          justifyCenter
          row
          style={{
            marginTop: moderateScale(35),
            backgroundColor: Colors.white,
            height: moderateScale(40),
          }}>
          <View
            style={{
              position: 'absolute',
              left: moderateScale(24),
            }}>
            <Loader />
          </View>
          <Text color="darkerGray" large>
            Logging in...
          </Text>
        </View>
      </View>
    </>
  );
};

let LoginAlerts = (props, ref) => {
  const {alertInfo, onSuccess, onError} = props;
  const modalRef = useRef();

  useImperativeHandle(ref, () => modalRef.current, []);

  const setShowAlert = (shouldShow) => {
    if (shouldShow) {
      modalRef.current.show();
    } else {
      modalRef.current.hide();
    }
  };

  const renderSetupDetails = () => {
    return (
      <>
        <View
          isFlex
          justifyCenter
          style={{
            paddingHorizontal: moderateScale(20),
            paddingVertical: moderateScale(20),
          }}>
          <Text
            center
            style={{
              marginBottom: 2,
            }}
            weight="medium"
            xlarge>
            Setup Details
          </Text>
          <Text center small>
            You can find both your App Name/ID
          </Text>
          <Text center small>
            and API key in your Macanta
          </Text>
          <Text center small>
            Desktop App, under
          </Text>
          <Text center small weight="semibold">
            Admin > Data Tools > API
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => setShowAlert(false)}
          style={styles.button}>
          <Text
            center
            style={{
              color: '#007AFF',
            }}
            xlarge>
            Got it, thanks
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  const renderConnectionSuccess = () => {
    return (
      <>
        <View
          isFlex
          justifyCenter
          style={{
            paddingHorizontal: moderateScale(20),
            paddingVertical: moderateScale(20),
          }}>
          <Text
            center
            style={{
              marginBottom: 2,
            }}
            weight="medium"
            xlarge>
            Connection Success
          </Text>
          <Text center small>
            Your mobile app has been
          </Text>
          <Text center small>
            successfully connected.
          </Text>
          <Text
            center
            small
            style={{
              marginTop: moderateScale(24),
            }}>
            You can now log in.
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => setShowAlert(false)}
          style={styles.button}>
          <Text
            center
            style={{
              color: '#007AFF',
            }}
            xlarge>
            Go to Login
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  return (
    <Alert
      disableBackdropPress={true}
      ref={modalRef}
      style={{
        height: moderateScale(178),
        // marginBottom: moderateScale(220),
      }}>
      {alertInfo.type === 'connecting' ? (
        <Connecting
          {...{onSuccess, onError, setShowAlert, payload: alertInfo.payload}}
        />
      ) : alertInfo.type === 'login' ? (
        <LoggingIn
          {...{onSuccess, onError, setShowAlert, payload: alertInfo.payload}}
        />
      ) : (
        renderSetupDetails()
      )}
    </Alert>
  );
};

const styles = StyleSheet.create({
  button: {
    borderTopColor: '#3C3C434A',
    borderTopWidth: 1,
    height: moderateScale(44),
    justifyContent: 'center',
    width: '100%',
  },
});

LoginAlerts = forwardRef(LoginAlerts);

LoginAlerts.defaultProps = {
  alertInfo: {},
  onSuccess: () => {},
  onError: () => {},
};

export default LoginAlerts;
