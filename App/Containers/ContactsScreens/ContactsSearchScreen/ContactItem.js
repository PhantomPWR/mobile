import React from 'react';
import {View} from '../../../Components/Layout';

import {Text} from '../../../Components/Text';
import {ListItem, IconButton} from '../../../Components/Buttons';
import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {call, sendSMS, sendEmail} from '../../../Utils/social';

export const ITEM_HEIGHT = moderateScale(Metrics.isAndroid ? 94 : 80, 0.4);

const Item = (props) => {
  const {contact, onPress} = props;
  const hasNoContactNumbers = !contact.phoneNumbers.length;

  return (
    <View
      style={{
        marginTop: moderateScale(8, 0.3),
        height: ITEM_HEIGHT,
        paddingHorizontal: Metrics.spaceHorizontal,

        elevation: 1,
        shadowColor: Colors.black,
        shadowRadius: 2,
        shadowOpacity: 0.06,
        shadowOffset: {
          width: 2,
          height: 2,
        },
      }}>
      <ListItem
        onPress={onPress}
        style={{
          borderRadius: moderateScale(4),
        }}>
        <>
          <View isFlex>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              weight="medium"
              xxlarge>
              {contact.firstName} {contact.lastName}
            </Text>
            <Text
              color="darkerGray"
              ellipsizeMode="tail"
              numberOfLines={1}
              small
              style={{
                marginTop: moderateScale(4, 0.3),
              }}
              weight="medium">
              {contact.email}
            </Text>
            <Text
              color="darkerGray"
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{
                marginTop: moderateScale(4, 0.3),
              }}
              xsmall>
              {contact.company || 'N/A'}
            </Text>
          </View>
          <View
            alignCenter
            row
            style={
              {
                // position: 'absolute',
                // right: Metrics.spaceHorizontal,
              }
            }>
            {!hasNoContactNumbers && (
              <>
                <IconButton
                  containerStyle={{
                    paddingHorizontal: 0,
                  }}
                  icon="Call"
                  iconFill={Colors.success}
                  iconSize={moderateScale(18, 0.3)}
                  onPress={() => {
                    console.tron.log('CALLING: ' + contact.phoneNumbers[0]);
                    call(contact.phoneNumbers[0]);
                  }}
                  style={{
                    // borderWidth: 1,
                    // borderColor: Colors.lighterGray,
                    // borderRadius: moderateScale(38, 0.3),
                    width: moderateScale(38, 0.3),
                    height: moderateScale(38, 0.3),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                />

                <IconButton
                  containerStyle={{
                    paddingHorizontal: 0,
                  }}
                  icon="Message"
                  iconFill={Colors.darkerGray}
                  iconSize={moderateScale(18, 0.3)}
                  onPress={() => {
                    console.tron.log('Send SMS: ' + contact.phoneNumbers);
                    sendSMS(contact.phoneNumbers);
                  }}
                  style={{
                    // borderWidth: 1,
                    // borderColor: Colors.lighterGray,
                    // borderRadius: moderateScale(38, 0.3),
                    width: moderateScale(38, 0.3),
                    height: moderateScale(38, 0.3),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                />
              </>
            )}
            <IconButton
              containerStyle={{
                paddingHorizontal: 0,
              }}
              icon="Email"
              iconFill={Colors.darkerGray}
              iconSize={moderateScale(18, 0.3)}
              onPress={() => {
                console.tron.log('Send Email: ' + contact.email);
                sendEmail(contact.email);
              }}
              style={{
                // borderWidth: 1,
                // borderColor: Colors.lighterGray,
                // borderRadius: moderateScale(38, 0.3),
                width: moderateScale(38, 0.3),
                height: moderateScale(38, 0.3),
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
        </>
      </ListItem>
    </View>
  );
};

Item.defaultProps = {};

export default Item;
