import React from 'react';

import {IconButton} from '../../../Components/Buttons';

import {Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const DeletePhoneButton = (props) => {
  const {style, containerStyle, onPress, ...allProps} = props;

  return (
    <IconButton
      containerStyle={[
        {
          width: moderateScale(32, 0.3),
          alignItems: 'center',
        },
        containerStyle,
      ]}
      icon="Subtract"
      iconFill={Colors.white}
      iconSize={moderateScale(12, 0.3)}
      onPress={onPress}
      style={[
        {
          backgroundColor: Colors.error,
          borderRadius: moderateScale(24, 0.3),
          paddingHorizontal: 0,
          width: moderateScale(24, 0.3),
          height: moderateScale(24, 0.3),
          alignItems: 'center',
          justifyContent: 'center',
        },
        style,
      ]}
      {...allProps}
    />
  );
};

export default DeletePhoneButton;
