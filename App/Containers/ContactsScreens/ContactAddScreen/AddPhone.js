import React from 'react';
import {StyleSheet} from 'react-native';
import {View} from '../../../Components/Layout';
import {IconButton} from '../../../Components/Buttons';
import {Text} from '../../../Components/Text';

import {Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const SWITCH_HEIGHT = moderateScale(60, 0.3);

const AddPhone = (props) => {
  const {label, onPress, style, ...allProps} = props;

  return (
    <View style={[styles.rowItem, style]} {...allProps}>
      <>
        <View
          isFlex
          style={{
            paddingRight: moderateScale(24),
          }}>
          <Text small>{label}</Text>
        </View>

        <IconButton
          icon="Plus"
          iconFill={Colors.white}
          iconSize={moderateScale(16, 0.3)}
          onPress={onPress}
          style={{
            backgroundColor: Colors.yellow,
            borderRadius: moderateScale(32, 0.3),
            paddingHorizontal: 0,
            width: moderateScale(32, 0.3),
            height: moderateScale(32, 0.3),
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      </>
    </View>
  );
};

const styles = StyleSheet.create({
  rowItem: {
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: SWITCH_HEIGHT,
    justifyContent: 'space-between',
  },
});

AddPhone.defaultProps = {
  onValueChange: () => {},
};

export default AddPhone;
