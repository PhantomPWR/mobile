import React, {useState, useRef, useEffect} from 'react';
import {ScrollView} from 'react-native';
import ScreenContainer from '../../Shared/ScreenContainer';
import {Content, View} from '../../../Components/Layout';
import {ErrorMessage} from '../../../Components/Text';
import Panel from '../../../Components/Forms/Panel';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import FormInput from '../../Shared/Forms/FormInput';

import SubmitButton from '../../Shared/Forms/SubmitButton';
import AddPhone from './AddPhone';
import DeletePhoneButton from './DeletePhoneButton';

import {contactValidationSchema} from '../../../Utils/validations';
import {Formik} from 'formik';
import {CREATE_OR_UPDATE_CONTACT} from '../../../GraphQL/Contacts';
import {useQuery} from '../../../Hooks';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {range} from 'lodash';
import {compose, dissoc} from 'ramda';

const MAX_PHONE_NUMBERS = 3;

const ContactAddScreen = (props) => {
  const {isEdit, contact = {phoneNumbers: []}} = props.route.params || {}; //{ allowMarketing: true }
  const [phoneAmt, setPhoneAmt] = useState(
    !contact.phoneNumbers.length
      ? [1]
      : range(1, contact.phoneNumbers.length + 1),
  );
  const lastNameRef = useRef();
  const companyRef = useRef();
  const emailRef = useRef();
  const titleRef = useRef();
  const submittedFormValues = useRef({});

  const {loading: loadingAddContact, error, data, callQuery} = useQuery(
    CREATE_OR_UPDATE_CONTACT,
  );

  useEffect(() => {
    if (!loadingAddContact && !error && data.createOrUpdateContact) {
      const responseContact = {
        id: data.createOrUpdateContact.id,
        ...submittedFormValues.current,
      };

      // if (isEdit) {
      //   setContact(responseContact);
      // }

      console.tron.log('SUCCESS SAVE CONTACT!!', data.createOrUpdateContact);

      props.navigation[isEdit ? 'navigate' : 'replace']('ContactDetails', {
        contact: responseContact,
        isContactSaved: true,
      });
    }
  }, [loadingAddContact]);

  return (
    <ScreenContainer
      headerTitle={`${!isEdit ? 'Add' : 'Edit'} Contact`}
      // style={{
      //   backgroundColor: Colors.white
      // }}
    >
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          minHeight: '100%',
          // paddingTop: moderateScale(16, 0.3)
        }}
        keyboardShouldPersistTaps="always">
        <Formik
          initialValues={contact}
          onSubmit={(values) => {
            submittedFormValues.current = values;

            const newContact = compose(
              dissoc('id'),
              dissoc('__typename'),
            )(values);

            console.tron.log('newContact', newContact);

            callQuery(newContact, 'createOrUpdateContactInput');
            // alert("In Progress")
          }}
          validateOnBlur={false}
          validateOnChange={false}
          validationSchema={contactValidationSchema}>
          {({handleChange, values, handleSubmit, errors, setFieldValue}) => (
            <>
              <PanelHeader label="General Information" />

              <Content
                isFlex={false}
                style={{
                  backgroundColor: Colors.white,
                }}>
                <FormInput
                  // autoFocus
                  autoCapitalize="words"
                  defaultValue={values.firstName}
                  error={errors.firstName}
                  label="First Name"
                  onChangeText={handleChange('firstName')}
                  onSubmitEditing={() => lastNameRef.current.focus()}
                  returnKeyType="next"
                />

                <FormInput
                  autoCapitalize="words"
                  defaultValue={values.lastName}
                  error={errors.lastName}
                  label="Last Name"
                  onChangeText={handleChange('lastName')}
                  onSubmitEditing={() => emailRef.current.focus()}
                  ref={lastNameRef}
                  returnKeyType="next"
                />

                <FormInput
                  autoCompleteType="email"
                  defaultValue={values.email}
                  error={errors.email}
                  keyboardType="email-address"
                  label="Email"
                  onChangeText={handleChange('email')}
                  onSubmitEditing={() => titleRef.current.focus()}
                  ref={emailRef}
                  returnKeyType="next"
                  textContentType="emailAddress"
                />

                <FormInput
                  autoCapitalize="words"
                  defaultValue={values.title}
                  error={errors.title}
                  label="Title"
                  onChangeText={handleChange('title')}
                  onSubmitEditing={() => companyRef.current.focus()}
                  ref={titleRef}
                  returnKeyType="next"
                />

                <FormInput
                  autoCapitalize="words"
                  defaultValue={values.company}
                  error={errors.company}
                  label="Company"
                  onChangeText={handleChange('company')}
                  ref={companyRef}
                  // onSubmitEditing={() => emailRef.current.focus()}
                  returnKeyType="next"
                />

                {phoneAmt.map((amt, index) => {
                  return (
                    <FormInput
                      autoFocus={
                        index !== 0 && index > contact.phoneNumbers.length - 1
                      }
                      // noClear
                      defaultValue={contact.phoneNumbers[index]}
                      error={errors[`phone${index + 1}`]}
                      // ref={phoneNumberRef}
                      key={amt}
                      keyboardType="number-pad"
                      label={`Phone ${index + 1}`}
                      onChangeText={(text) =>
                        setFieldValue(`phoneNumbers[${index}]`, text)
                      }
                      returnKeyType="next"
                      {...(phoneAmt.length > 1 && {
                        clearStyle: {
                          right: moderateScale(32, 0.3),
                        },
                      })}>
                      {phoneAmt.length > 1 && (
                        <DeletePhoneButton
                          containerStyle={{
                            height: 'auto',
                            paddingHorizontal: 0,
                            position: 'absolute',
                            top: moderateScale(8, 0.3),
                            bottom: 0,
                            right: 0,
                          }}
                          onPress={() => {
                            const nextVal = contact.phoneNumbers[index + 1];
                            console.tron.log('nextVal: ' + nextVal);
                            if (nextVal) {
                              setFieldValue(`phoneNumbers[${index}]`, nextVal);
                              setFieldValue(`phoneNumbers[${index + 1}]`, null);
                            }

                            setPhoneAmt((prevState) => {
                              const prevPhoneAmt = prevState.slice();
                              prevPhoneAmt.splice(index, 1);

                              return prevPhoneAmt;
                            });
                          }}
                        />
                      )}
                    </FormInput>
                  );
                })}

                {phoneAmt.length < MAX_PHONE_NUMBERS && (
                  <AddPhone
                    label="Add another phone number"
                    onPress={() =>
                      setPhoneAmt((prevState) =>
                        prevState.concat(
                          (prevState[prevState.length - 1] || 0) + 1,
                        ),
                      )
                    }
                  />
                )}
              </Content>

              <Panel isAccordion={true} label="Billing Address">
                <Content
                  isFlex={false}
                  style={{
                    backgroundColor: Colors.white,
                  }}>
                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.streetAddress1}
                    error={errors.streetAddress1}
                    label="Street Address 1"
                    onChangeText={handleChange('streetAddress1')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.streetAddress2}
                    error={errors.streetAddress2}
                    label="Street Address 2"
                    onChangeText={handleChange('streetAddress2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.city}
                    error={errors.city}
                    label="City"
                    onChangeText={handleChange('city')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.state}
                    error={errors.state}
                    label="State"
                    onChangeText={handleChange('state')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.postalCode}
                    error={errors.postalCode}
                    label="Postal Code"
                    onChangeText={handleChange('postalCode')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.country}
                    error={errors.country}
                    label="Country"
                    onChangeText={handleChange('country')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />
                </Content>
              </Panel>

              <Panel isAccordion={true} label="Shipping Address">
                <Content
                  isFlex={false}
                  style={{
                    backgroundColor: Colors.white,
                  }}>
                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.address2Street1}
                    error={errors.address2Street1}
                    label="Street Address 1"
                    onChangeText={handleChange('address2Street1')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.address2Street2}
                    error={errors.address2Street2}
                    label="Street Address 2"
                    onChangeText={handleChange('address2Street2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.city2}
                    error={errors.city2}
                    label="City"
                    onChangeText={handleChange('city2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.state2}
                    error={errors.state2}
                    label="State"
                    onChangeText={handleChange('state2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.postalCode2}
                    error={errors.postalCode2}
                    label="Postal Code"
                    onChangeText={handleChange('postalCode2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />

                  <FormInput
                    // autoFocus
                    autoCapitalize="words"
                    defaultValue={values.country2}
                    error={errors.country2}
                    label="Country"
                    onChangeText={handleChange('country2')}
                    // onSubmitEditing={() => lastNameRef.current.focus()}
                    returnKeyType="next"
                  />
                </Content>
              </Panel>

              <SubmitButton
                isLoading={loadingAddContact}
                onPress={handleSubmit}
                title="SAVE"
              />

              <View
                isFlex
                justifyEnd
                style={{
                  paddingVertical: moderateScale(20, 0.3),
                  paddingHorizontal: Metrics.spaceHorizontal,
                  // backgroundColor: Colors.background
                }}>
                <ErrorMessage center>{error}</ErrorMessage>
              </View>
            </>
          )}
        </Formik>
      </ScrollView>
    </ScreenContainer>
  );
};

export default ContactAddScreen;
