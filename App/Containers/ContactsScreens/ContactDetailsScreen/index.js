import React, {useCallback, useEffect, useState} from 'react';

import ContactContext from './ContactContext';
import ContactDetails from './ContactDetails';

const ContactDetailsScreen = (props) => {
  const {contact: contactProp, isContactSaved} = props.route.params;
  const [contact, setContact] = useState(contactProp);
  const [initialDataLoaded, setInitialDataLoaded] = useState([]);
  const [hasContactSaved, setHasContactSaved] = useState(false);

  const setInitialDataLoadedFn = useCallback(
    (type) => {
      if (!initialDataLoaded.includes(type)) {
        setInitialDataLoaded(initialDataLoaded.concat(type));
      }
    },
    [initialDataLoaded],
  );

  const handleBack = useCallback(() => {
    props.navigation.navigate('Contacts', {hasContactSaved});
  }, [props.navigation, hasContactSaved]);

  const isInitialLoadingAllData =
    !initialDataLoaded.includes('note') ||
    !initialDataLoaded.includes('task') ||
    !initialDataLoaded.includes('dataObject');

  useEffect(() => {
    setContact(contactProp);
  }, [contactProp]);

  useEffect(() => {
    if (isContactSaved) {
      setHasContactSaved(true);
    }
  }, [isContactSaved]);

  return (
    <ContactContext.Provider
      value={{
        contact,
        setInitialDataLoaded: setInitialDataLoadedFn,
      }}>
      <ContactDetails
        contact={contact}
        isInitialLoadingAllData={isInitialLoadingAllData}
        onBack={handleBack}
      />
    </ContactContext.Provider>
  );
};

export default ContactDetailsScreen;
