import React from 'react';

const ContactContext = React.createContext({
  contact: {},
  setInitialDataLoaded: () => {},
});

export default ContactContext;
