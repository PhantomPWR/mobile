import React, {useRef} from 'react';
import {
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import ScreenContainer from '../../Shared/ScreenContainer';
import {View, ToggleView} from '../../../Components/Layout';
import {Text} from '../../../Components/Text';
import {IconButton} from '../../../Components/Buttons';
import SvgIcon from '../../../Components/SvgIcon';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import ContactTabNavigation from '../../../Navigation/ContactTabNavigation';
import Loader from '../../../Components/Loader';
import {useNavigation} from '@react-navigation/native';
import {call, sendEmail} from '../../../Utils/social';
import {useWindowDimensions} from '../../../Hooks';

const BAR_MARGIN_TOP = moderateScale(14, 0.4);

const Small = (props) => {
  const {children, label, ...allProps} = props;

  // if(!children) {
  //   return null
  // }
  return (
    <Text
      color="white"
      small
      style={{
        marginBottom: moderateScale(4, 0.4),
      }}
      weight="semibold"
      {...allProps}>
      {label ? `${label}: ` : ''}
      <Text color="white">&nbsp;{children || 'n/a'}</Text>
    </Text>
  );
};

const Info = (props) => {
  const {
    icon,
    iconSize = moderateScale(14, 0.1),
    iconFill = Colors.white,
    label,
    labelProps = {small: true},
    onPress,
    ...allProps
  } = props;

  if (!label) {
    return null;
  }

  const InfoContainer = onPress
    ? (infoProps) => <TouchableOpacity onPress={onPress} {...infoProps} />
    : View;

  return (
    <InfoContainer
      style={{
        alignSelf: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: moderateScale(Metrics.isAndroid ? 4 : 8, 0.4),
      }}
      {...allProps}>
      <SvgIcon fill={iconFill} height={iconSize} name={icon} width={iconSize} />
      <Text
        color="white"
        style={{
          marginLeft: moderateScale(12, 0.1),
        }}
        {...labelProps}>
        {label}
      </Text>
    </InfoContainer>
  );
};

const ContactDetails = (props) => {
  const {isInitialLoadingAllData, contact, onBack} = props;
  const navigation = useNavigation();
  const {windowHeight} = useWindowDimensions();

  console.tron.log('ContactDetails', contact);

  const toggleViewRef = useRef();
  const animatedIsVisible = useRef(new Animated.Value(0)).current;

  return (
    <ScreenContainer
      barStyle={{
        marginTop: BAR_MARGIN_TOP,
      }}
      headerCenter={
        <View
          // isFlex
          style={{
            // backgroundColor: 'pink',
            marginTop: moderateScale(14, 0.4),
            marginBottom: moderateScale(6, 0.4),
            paddingHorizontal: Metrics.spaceHorizontal,
          }}>
          <Info
            icon="Email"
            label={contact.email}
            onPress={() => {
              console.tron.log('Send Email: ' + contact.email);
              sendEmail(contact.email);
            }}
          />

          <Info
            icon="Call"
            label={contact.phoneNumbers[0]}
            onPress={() => {
              console.tron.log('CALLING: ' + contact.phoneNumbers[0]);
              call(contact.phoneNumbers[0]);
            }}
          />

          <Info icon="Company" label={contact.company} />

          <ToggleView
            animatedIsVisible={animatedIsVisible}
            isAnimated
            ref={toggleViewRef}
            style={{
              backgroundColor: Colors.primary,
              paddingHorizontal: Metrics.spaceHorizontal,
              paddingBottom: moderateScale(12, 0.4),
              opacity: animatedIsVisible.interpolate({
                inputRange: [0.4, 0.6],
                outputRange: [0, 1],
                extrapolate: 'clamp',
              }),
            }}
            toggle={(onPress) => (
              <IconButton
                containerStyle={{
                  // backgroundColor: 'blue',
                  position: 'absolute',
                  top: 0,
                  bottom: moderateScale(6, 0.4),
                  right: 0,
                  justifyContent: 'flex-end',
                  // paddingHorizontal: 0,
                  height: 'auto',
                  zIndex: 1000,
                }}
                icon="DownChevron"
                iconFill={Colors.white}
                iconSize={moderateScale(14, 0.1)}
                isAnimated
                onPress={onPress}
                style={{
                  backgroundColor: Colors.lightYellow,
                  borderRadius: moderateScale(26, 0.3),
                  width: moderateScale(26, 0.3),
                  height: moderateScale(26, 0.3),
                  alignItems: 'center',
                  justifyContent: 'center',
                  transform: [
                    {
                      rotate: animatedIsVisible.interpolate({
                        inputRange: [0, 1],
                        outputRange: ['0deg', '180deg'],
                        extrapolate: 'clamp',
                      }),
                    },
                  ],
                }}
              />
            )}>
            <View
              style={{
                marginTop: moderateScale(14, 0.4),
              }}>
              <Info
                icon="BillingAddress"
                iconSize={moderateScale(20, 0.1)}
                label="Billing Address"
                labelProps={{
                  slarge: true,
                  weight: 'semibold',
                }}
              />

              <View>
                <Small label="Address 1">{contact.streetAddress1}</Small>
                <Small label="Address 2">{contact.streetAddress2}</Small>
                <Small label="City">{contact.city}</Small>
                <Small label="State">{contact.state}</Small>
                <Small label="Postal Code">{contact.postalCode}</Small>
                <Small label="Country">{contact.country}</Small>
              </View>
            </View>

            <View
              style={{
                marginTop: moderateScale(14, 0.4),
              }}>
              <Info
                icon="ShippingAddress"
                iconSize={moderateScale(20, 0.1)}
                label="Shipping Address"
                labelProps={{
                  slarge: true,
                  weight: 'semibold',
                }}
              />

              <View>
                <Small label="Address 1">{contact.address2Street1}</Small>
                <Small label="Address 2">{contact.address2Street2}</Small>
                <Small label="City">{contact.city2}</Small>
                <Small label="State">{contact.state2}</Small>
                <Small label="Postal Code">{contact.postalCode2}</Small>
                <Small label="Country">{contact.country2}</Small>
              </View>
            </View>
          </ToggleView>
        </View>
      }
      headerRight={
        <IconButton
          icon="Edit"
          iconFill={Colors.white}
          iconSize={moderateScale(24, 0.4)}
          onPress={() =>
            navigation.navigate('ContactAdd', {
              isEdit: true,
              contact,
            })
          }
        />
      }
      headerStyle={{
        borderBottomWidth: 0,
      }}
      headerTitle={`${contact.firstName}${
        contact.lastName ? ' ' + contact.lastName : ''
      }`}
      onBack={onBack}>
      <ContactTabNavigation />

      {isInitialLoadingAllData && (
        <View
          alignCenter
          style={{
            ...Metrics.absoluteFill,
            paddingTop: moderateScale(20, 0.4),
            backgroundColor: Colors.background,
          }}>
          <Loader />
        </View>
      )}

      <TouchableWithoutFeedback onPress={() => toggleViewRef.current.toggle()}>
        <Animated.View
          style={{
            // zIndex: -1,
            ...Metrics.absoluteFill,
            opacity: animatedIsVisible.interpolate({
              inputRange: [0.1, 1],
              outputRange: [0, 1],
              extrapolate: 'clamp',
            }),
            transform: [
              {
                translateY: animatedIsVisible.interpolate({
                  inputRange: [0, 0.1],
                  outputRange: [windowHeight, 0],
                  extrapolate: 'clamp',
                }),
              },
            ],
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
          }}
        />
      </TouchableWithoutFeedback>
    </ScreenContainer>
  );
};

export default ContactDetails;
