import React, {useEffect, useState} from 'react';
import {Image, ScrollView, TouchableOpacity} from 'react-native';
import {Content, View} from '../../../Components/Layout';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import {Label, Text} from '../../../Components/Text';
import Loader from '../../../Components/Loader';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByKeyCondition} from '../../../Utils/array';
import {moderateScale} from 'react-native-size-matters/extend';
import {RectButton} from 'react-native-gesture-handler';

const THUMBNAIL_SIZE = moderateScale(64, 0.1);

const downloadFile = (url, localFile) => {
  const options = {
    fromUrl: url,
    toFile: localFile,
  };
  return RNFS.downloadFile(options).promise;
};

const viewFile = (localFile) => {
  return FileViewer.open(localFile, {showOpenWithDialog: true});
};

const File = ({attachment}) => {
  const {fileName, thumbnail, downloadUrl} = attachment;
  console.tron.log('file', fileName, thumbnail, downloadUrl);
  const [loading, setLoading] = useState(false);

  const handlePressFile = async () => {
    setLoading(true);
    const localFile = `${RNFS.TemporaryDirectoryPath}/${fileName}`;

    try {
      console.tron.log('localFile: ' + localFile);
      await downloadFile(downloadUrl, localFile);
      await viewFile(localFile);
      console.tron.log('SUCCESS DOWNLOAD AND OPEN FILE: ' + downloadUrl);
    } catch (err) {
      console.tron.log('ERROR on viewing attachments');
      console.tron.log(err);
    }

    setLoading(false);
  };

  return (
    <TouchableOpacity
      disabled={loading}
      onPress={handlePressFile}
      style={{
        alignItems: 'center',
        backgroundColor: Colors.lightestGray,
        borderRadius: moderateScale(4),
        marginTop: moderateScale(8, 0.1),
        padding: moderateScale(16, 0.1),

        elevation: 1,
        shadowColor: Colors.black,
        shadowRadius: 2,
        shadowOpacity: 0.16,
        shadowOffset: {
          width: 2,
          height: 2,
        },
      }}>
      <>
        <Image
          resizeMode="contain"
          source={{
            uri: `data:image/png;base64, ${thumbnail}`,
          }}
          style={{width: THUMBNAIL_SIZE, height: THUMBNAIL_SIZE}}
        />
        <Text
          center
          color="primary"
          style={{
            marginTop: moderateScale(12, 0.1),
          }}
          weight="semibold"
          xsmall>
          {fileName}
        </Text>

        {loading && (
          <View
            alignCenter
            justifyCenter
            style={{
              ...Metrics.absoluteFill,
              borderRadius: moderateScale(4),
              backgroundColor: 'rgba(0, 0, 0, 0.4)',
            }}>
            <Loader color={Colors.white} size="small" />
          </View>
        )}
      </>
    </TouchableOpacity>
  );
};
const Attachments = ({attachments}) => {
  return (
    <ScrollView
      contentContainerStyle={{
        paddingTop: moderateScale(8, 0.1),
        paddingBottom: moderateScale(10, 0.1),
      }}>
      <Content>
        {attachments.map(({id, ...attachment}) => {
          return <File attachment={attachment} key={id} />;
        })}
      </Content>
    </ScrollView>
  );
};

export default Attachments;
