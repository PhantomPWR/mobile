import React from 'react';
import {FlatList} from '../../../Components/Layout';
import {LIST_DATA_OBJECT_ITEMS} from '../../../GraphQL/DataObjects';
import {useReactiveVar, useQuery} from '../../../Hooks';
import ScreenContainer from '../../Shared/ScreenContainer';
import DataObjectItem from './Item';
import {moderateScale} from 'react-native-size-matters/extend';
import {Metrics} from '../../../Themes';
import {userDetailsVar} from '../../../Cache';

const DataObjectItemsScreen = (props) => {
  const {doId, doTitle, contactId} = props.route.params;
  const {email} = useReactiveVar(userDetailsVar);

  const {loading, error, data, callQuery} = useQuery(LIST_DATA_OBJECT_ITEMS, {
    groupId: doId,
    contactId,
    email,
  });

  console.tron.log('dataObject items', loading, error, data);

  const reloadData = () => {
    console.tron.log('onRefresh Data Object Items reloadData');
    callQuery({groupId: doId, contactId, email});
  };

  return (
    <ScreenContainer
      headerTitle={doTitle}
      // headerRight={
      //   <IconButton
      //     onPress={() => {}}
      //     icon='Plus'
      //     iconFill={Colors.white}
      //   />
      // }
    >
      <FlatList
        contentContainerStyle={{
          paddingTop: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
        }}
        data={data.listDataObjectItems && data.listDataObjectItems.items}
        keyExtractor={(item, index) => item.id || String(index)}
        loading={loading}
        onRefresh={() => reloadData()}
        renderItem={({item}) => (
          <DataObjectItem
            fields={data.listDataObjectItems && data.listDataObjectItems.fields}
            item={item}
          />
        )}
      />
    </ScreenContainer>
  );
};

DataObjectItemsScreen.defaultProps = {};

export default DataObjectItemsScreen;
