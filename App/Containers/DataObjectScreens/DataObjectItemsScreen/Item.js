import React from 'react';
import {View, Content} from '../../../Components/Layout';
import {Text} from '../../../Components/Text';
import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import DOItemShowMore from './DOItemShowMore';

const CONTAINER_VERTICAL_PADDING = moderateScale(16, 0.1);

const Item = (props) => {
  const {item, fields} = props;

  const selectedFieldLength = fields.filter((field) => field.showInTable)
    .length;
  const fieldLength =
    selectedFieldLength || (fields.length > 3 ? 3 : fields.length);
  const fieldsData = fields.slice(0, fieldLength);
  const dataSliced = item.data.slice(0, fieldLength);

  return (
    <View
      style={{
        backgroundColor: Colors.white,
        paddingVertical: CONTAINER_VERTICAL_PADDING,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        marginBottom: moderateScale(8, 0.3),
        height: moderateScale(
          (Metrics.isAndroid ? 130 : 90) + 50 * fieldLength,
          0.3,
        ),
      }}>
      <Content>
        <View
          // row
          // justifyBetween
          alignCenter>
          <Text color="primary" weight="medium">
            ID: {item.id}
          </Text>
        </View>

        {/* <View style={{
          height: 1,
          width: '100%',
          backgroundColor: Colors.border,
          marginVertical: INNER_VERTICAL_PADDING
        }}/> */}

        <View
          style={{
            overflow: 'hidden',
            // backgroundColor: 'red'
            marginTop: moderateScale(2, 0.1),
          }}>
          {fieldsData.map((field, index) => {
            const itemData =
              dataSliced.find((d) => d.fieldId === field.fieldId) || {};

            return (
              <View
                key={field.fieldId}
                style={{
                  paddingVertical: moderateScale(10, 0.2),
                  borderBottomWidth: 1,
                  borderBottomColor: Colors.border,
                }}>
                <View
                  style={
                    {
                      // flex: 0.4,
                      // backgroundColor: 'pink'
                    }
                  }>
                  <Text numberOfLines={1} small weight="semibold">
                    {field.key}
                  </Text>
                </View>
                <View
                  style={
                    {
                      // flex: 0.6,
                      // backgroundColor: 'orange'
                    }
                  }>
                  <Text
                    numberOfLines={1}
                    xsmall
                    {...(!itemData.value && {color: 'darkerGray'})}>
                    {itemData.value || 'N/A'}
                  </Text>
                </View>
              </View>
            );
          })}
        </View>

        <DOItemShowMore fields={fields} item={item} />
      </Content>
    </View>
  );
};

Item.defaultProps = {};

export default Item;
