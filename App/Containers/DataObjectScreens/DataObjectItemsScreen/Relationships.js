import React, {Fragment, useEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import {Content, View} from '../../../Components/Layout';
import {userDetailsVar} from '../../../Cache';
import {useReactiveVar} from '../../../Hooks';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import {Label, Text} from '../../../Components/Text';
import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByKeyCondition} from '../../../Utils/array';
import {moderateScale} from 'react-native-size-matters/extend';

const ConnectedContact = ({label, relationships}) => {
  return (
    <View
      style={{
        paddingVertical: moderateScale(10, 0.1),
      }}>
      <Text medium weight="medium">
        {label}
      </Text>
      <Text color="darkerGray" small>
        {relationships.map((relationship) => relationship.role).join(', ')}
      </Text>
    </View>
  );
};

const Relationships = ({connectedContacts}) => {
  const userDetails = useReactiveVar(userDetailsVar);

  const [sortedContacts, setSortedContacts] = useState([]);

  useEffect(() => {
    setSortedContacts(
      sortArrayByKeyCondition(
        connectedContacts,
        'email',
        (value) => value === userDetails.email,
      ),
    );
  }, [connectedContacts, userDetails.email]);

  return (
    <ScrollView
      contentContainerStyle={{
        paddingTop: moderateScale(8, 0.1),
        paddingBottom: moderateScale(10, 0.1),
      }}>
      <Content>
        {sortedContacts.map(
          ({id, email, firstName, lastName, relationships}) => {
            const isLoggedInUser = email === userDetails.email;
            const label = isLoggedInUser
              ? 'My Relationship'
              : firstName + ' ' + lastName;
            return (
              <Fragment key={id}>
                <ConnectedContact
                  key={id}
                  label={label}
                  relationships={relationships}
                />
                {isLoggedInUser && (
                  <PanelHeader
                    label="Other Related Contacts"
                    labelProps={{
                      weight: 'normal',
                    }}
                    labelStyle={{
                      textTransform: 'capitalize',
                      color: Colors.lightGray,
                      fontSize: moderateScale(
                        ApplicationStyles.fontSizes.xsmall,
                        0.3,
                      ),
                    }}
                    style={{
                      backgroundColor: Colors.white,
                      borderBottomColor: Colors.border,
                      height: ApplicationStyles.panelHeader.height * 0.8,
                      paddingHorizontal: 0,
                    }}
                  />
                )}
              </Fragment>
            );
          },
        )}
      </Content>
    </ScrollView>
  );
};

export default Relationships;
