import React, {useRef} from 'react';
import {TouchableOpacity} from 'react-native';
import {View, Modal} from '../../../Components/Layout';
import SvgIcon from '../../../Components/SvgIcon';
import {Text} from '../../../Components/Text';
import {IconButton} from '../../../Components/Buttons';
import {Metrics, Colors} from '../../../Themes';
import DataObjectTabNavigation from '../../../Navigation/DataObjectTabNavigation';
import {moderateScale} from 'react-native-size-matters/extend';

const DOItemShowMore = (props) => {
  const {item} = props;
  const modalRef = useRef();

  const handleShow = async (shouldShow) => {
    if (shouldShow) {
      modalRef.current.show();
    } else {
      modalRef.current.hide();
    }
  };

  return (
    <>
      <TouchableOpacity
        onPress={() => handleShow(true)}
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          flexDirection: 'row',
          alignItems: 'center',
          // backgroundColor: 'red',
          paddingTop: moderateScale(20, 0.1),
          paddingHorizontal: Metrics.spaceHorizontal,
        }}>
        <Text
          color="yellow"
          style={{
            marginRight: moderateScale(4, 0.3),
          }}
          xxsmall>
          Show more
        </Text>
        <SvgIcon
          fill={Colors.yellow}
          height={moderateScale(10, 0.3)}
          name="DownChevron"
          width={moderateScale(10, 0.3)}
        />
      </TouchableOpacity>

      <Modal alignCenter justifyCenter ref={modalRef}>
        <View
          style={{
            backgroundColor: Colors.white,
            borderRadius: 14,
            overflow: 'hidden',
            width: Metrics.isTablet
              ? moderateScale(320)
              : Metrics.wp(100) - Metrics.spaceHorizontal * 2,
            paddingTop: moderateScale(8, 0.3),
          }}>
          <View alignCenter justifyBetween row>
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                alignItems: 'center',
              }}>
              <Text center color="primary" weight="semibold" xlarge>
                {item.id}
              </Text>
            </View>

            <IconButton
              containerStyle={{
                alignSelf: 'flex-end',
                alignItems: 'flex-end',
              }}
              icon="Close"
              iconFill={Colors.darkerGray}
              iconSize={moderateScale(16, 0.1)}
              isAnimated
              onPress={() => handleShow(false)}
            />
          </View>

          <View
            style={{
              height: Metrics.hp(74),
            }}>
            <DataObjectTabNavigation
              attachments={item.attachments}
              connectedContacts={item.connectedContacts}
              items={item.data}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

export default DOItemShowMore;
