import {convertWithSections, transformSectionsData} from './Details';

describe('DO Item Details Data Transformer', () => {
  let itemData;

  const initialValues = [
    {
      fieldId: 'field_kcylb25x',
      name: 'Project',
      type: 'Text',
      value: 'Macanta Apple Watch',
      subGroup: '',
      sectionTag: '',
    },
    {
      fieldId: 'field_kejv78th',
      name: 'Level',
      type: 'Text',
      value: 'Senior',
      subGroup: 'Section details 1',
      sectionTag: '',
    },
    {
      fieldId: 'field_kelg4y6m',
      name: 'Preferred Libraries',
      type: 'Text',
      value: 'Redux Toolkit',
      subGroup: '',
      sectionTag: '',
    },
    {
      fieldId: 'field_keljzr6m',
      name: 'Logged Hours',
      type: 'Number',
      value: '36',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 1',
    },
    {
      fieldId: 'field_kewffahy',
      name: 'Is Contact App Owner?',
      type: 'Text',
      value: 'Nope',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 1',
    },
    {
      fieldId: 'field_kg22lshb',
      name: 'Test Field with Section',
      type: 'Text',
      value: 'test',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 2',
    },
    {
      fieldId: 'field_kg22y8yk',
      name: 'Test Field 2',
      type: 'Text',
      value: '',
      subGroup: 'Section details 2',
      sectionTag: 'Test Section 2',
    },
  ];

  beforeEach(() => {
    itemData = initialValues;
  });

  it('happy - convert DO item data with sections and subGroups as object', () => {
    const actualResult = convertWithSections(itemData);
    const expectedResult = {
      General: {
        subGroups: {
          subGroupWithoutName: initialValues.filter(
            (item) => !item.sectionTag && !item.subGroup,
          ),
          'Section details 1': initialValues.filter(
            (item) => !item.sectionTag && item.subGroup === 'Section details 1',
          ),
        },
      },
      'Test Section 1': {
        subGroups: {
          'Section details 1': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 1' &&
              item.subGroup === 'Section details 1',
          ),
        },
      },
      'Test Section 2': {
        subGroups: {
          'Section details 1': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 2' &&
              item.subGroup === 'Section details 1',
          ),
          'Section details 2': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 2' &&
              item.subGroup === 'Section details 2',
          ),
        },
      },
    };

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - transform DO item data as sections and subGroups as array', () => {
    const sectionsObj = convertWithSections(itemData);
    const actualResult = transformSectionsData(sectionsObj);
    const expectedResult = [
      {
        sectionName: 'General',
        subGroups: [
          {
            subGroupName: 'subGroupWithoutName',
            data: [
              {
                fieldId: 'field_kcylb25x',
                fieldName: 'Project',
                value: 'Macanta Apple Watch',
              },
              {
                fieldId: 'field_kelg4y6m',
                fieldName: 'Preferred Libraries',
                value: 'Redux Toolkit',
              },
            ],
          },
          {
            subGroupName: 'Section details 1',
            data: [
              {fieldId: 'field_kejv78th', fieldName: 'Level', value: 'Senior'},
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 1',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                fieldId: 'field_keljzr6m',
                fieldName: 'Logged Hours',
                value: '36',
              },
              {
                fieldId: 'field_kewffahy',
                fieldName: 'Is Contact App Owner?',
                value: 'Nope',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 2',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                fieldId: 'field_kg22lshb',
                fieldName: 'Test Field with Section',
                value: 'test',
              },
            ],
          },
          {
            subGroupName: 'Section details 2',
            data: [
              {fieldId: 'field_kg22y8yk', fieldName: 'Test Field 2', value: ''},
            ],
          },
        ],
      },
    ];

    expect(actualResult).toEqual(expectedResult);
  });
});
