import React, {useEffect, useRef, useState} from 'react';
import {ScrollView} from 'react-native';
import {View, Content} from '../../../Components/Layout';

import Panel from '../../../Components/Forms/Panel';
import PanelHeader from '../../../Components/Forms/Panel/PanelHeader';
import {Text} from '../../../Components/Text';

import {ApplicationStyles, Metrics, Colors} from '../../../Themes';
import {sortArrayByPriority} from '../../../Utils/array';

import {moderateScale} from 'react-native-size-matters/extend';
import {Select} from '../../../Components/Forms';

const SECTION_FILTER_HEIGHT = moderateScale(38, 0.3);

export const convertWithSections = (itemData) => {
  return itemData.reduce((acc, d) => {
    const sections = {...acc};
    const sectionName = d.sectionTag || 'General';
    if (!sections[sectionName]) {
      sections[sectionName] = {subGroups: {}};
    }

    const section = sections[sectionName];
    const subGroups = section.subGroups;
    const subGroupName = d.subGroup || 'subGroupWithoutName';
    if (!subGroups[subGroupName]) {
      subGroups[subGroupName] = [];
    }

    const subGroup = subGroups[subGroupName];
    subGroup.push(d);

    return sections;
  }, {});
};

export const transformSectionsData = (sectionsObj) => {
  const transformedSectionsData = Object.entries(sectionsObj).map(
    ([sectionName, sectionData]) => {
      const transformedSubGroups = Object.entries(sectionData.subGroups).map(
        ([subGroupName, data]) => {
          return {
            subGroupName,
            data: data.map((d) => ({
              fieldId: d.fieldId,
              fieldName: d.name,
              value: d.value,
            })),
          };
        },
      );
      const sortedSubGroups = sortArrayByPriority(
        transformedSubGroups,
        'subGroupName',
        ['subGroupWithoutName'],
      );

      return {
        sectionName,
        subGroups: sortedSubGroups,
      };
    },
  );

  const sortedSectionsData = sortArrayByPriority(
    transformedSectionsData,
    'sectionName',
    ['sectionWithoutName'],
  );

  return sortedSectionsData;
};

const getSectionsData = (itemData) => {
  const sectionsObj = convertWithSections(itemData);

  const transformedSectionsData = transformSectionsData(sectionsObj);

  return transformedSectionsData;
};

const Details = (props) => {
  const {item} = props;
  const scrollRef = useRef(null);
  const [sectionsData, setSectionsData] = useState([]);
  const [sectionNames, setSectionNames] = useState([]);
  const [selectedFilter, setSelectedFilter] = useState('All');
  const [filteredSectionsData, setFilteredSectionsData] = useState([]);

  const handleFilterBySection = (value) => {
    setSelectedFilter(value);
    scrollRef.current.scrollTo({
      y: 0,
      animated: false,
    });
  };

  useEffect(() => {
    const allSections = getSectionsData(item.data);
    const allSectionNames = allSections.map((section) => section.sectionName);

    setSectionsData(allSections);
    setSectionNames(['All', ...allSectionNames]);
  }, [item.data]);

  useEffect(() => {
    if (selectedFilter !== 'All') {
      const filteredData = sectionsData.filter(
        (section) => section.sectionName === selectedFilter,
      );

      setFilteredSectionsData(filteredData);
    } else {
      setFilteredSectionsData(sectionsData);
    }
  }, [selectedFilter, sectionsData, setFilteredSectionsData]);

  if (!sectionsData.length) {
    return null;
  }

  return (
    <View isFlex>
      <Select
        items={sectionNames}
        labelPrefix="Section: "
        listTop={SECTION_FILTER_HEIGHT}
        onSelectPickerItem={handleFilterBySection}
        style={{
          paddingLeft: moderateScale(12, 0.3),
          paddingRight: Metrics.spaceHorizontal,
          height: SECTION_FILTER_HEIGHT,
          justifyContent: 'flex-end',
        }}
      />

      <ScrollView
        contentContainerStyle={{
          paddingBottom: moderateScale(10, 0.1),
        }}
        ref={scrollRef}>
        {filteredSectionsData.map((section) => {
          return (
            <Panel
              isAccordion={true}
              key={section.sectionName}
              label={section.sectionName}
              style={{
                // backgroundColor: Colors.white,
                // paddingHorizontal: 0,
                borderBottomWidth: 1,
                borderBottomColor: Colors.border,
                // borderTopWidth: 1,
                // borderTopColor: Colors.border,
              }}>
              {section.subGroups.map((subGroup) => {
                return (
                  <Content>
                    {subGroup.subGroupName !== 'subGroupWithoutName' && (
                      <PanelHeader
                        label={subGroup.subGroupName}
                        labelProps={{
                          weight: 'normal',
                        }}
                        labelStyle={{
                          textTransform: 'uppercase',
                          color: Colors.mutedText,
                          marginTop: moderateScale(15, 0.1),
                          fontWeight: 'bold',
                          fontSize: moderateScale(
                            ApplicationStyles.fontSizes.xsmall,
                            // 0.3,
                            0.5,
                          ),
                        }}
                        style={{
                          backgroundColor: Colors.white,
                          borderBottomColor: Colors.border,
                          height: ApplicationStyles.panelHeader.height * 0.8,
                          paddingHorizontal: 0,
                        }}
                      />
                    )}
                    {subGroup.data.map((itemData) => {
                      return (
                        <View
                          key={itemData.fieldId}
                          style={{
                            paddingVertical: moderateScale(10, 0.1),
                            // borderBottomWidth: 1,
                            // borderBottomColor: Colors.border,
                          }}>
                          <View
                            style={
                              {
                                // flex: 0.4,
                                // backgroundColor: 'pink'
                              }
                            }>
                            <Text small weight="semibold">
                              {itemData.fieldName}
                            </Text>
                          </View>
                          <View
                            style={
                              {
                                // flex: 0.6,
                                // backgroundColor: 'orange'
                              }
                            }>
                            <Text
                              xsmall
                              {...(!itemData.value && {color: 'gray'})}>
                              {itemData.value || 'N/A'}
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                  </Content>
                );
              })}
            </Panel>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default Details;
