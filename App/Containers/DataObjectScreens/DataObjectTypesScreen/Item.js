import React from 'react';
import {TouchableHighlight} from 'react-native';

import {Text} from '../../../Components/Text';

import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {useNavigation} from '@react-navigation/native';

const VERTICAL_PADDING = moderateScale(16, 0.1);

const Item = (props) => {
  const navigation = useNavigation();
  const {item, contactId} = props;

  return (
    <TouchableHighlight
      onPress={() =>
        navigation.navigate('DataObjectItems', {
          doId: item.id,
          doTitle: item.title,
          contactId,
        })
      }
      style={{
        backgroundColor: Colors.white,
        paddingHorizontal: Metrics.spaceHorizontal,
        paddingVertical: VERTICAL_PADDING,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        marginBottom: VERTICAL_PADDING,
        borderRadius: moderateScale(4),

        elevation: 1,
        shadowColor: Colors.black,
        shadowRadius: 2,
        shadowOpacity: 0.06,
        shadowOffset: {
          width: 2,
          height: 2,
        },
      }}
      underlayColor={Colors.silver}>
      <Text center color="primary" weight="medium" xlarge>
        {item.title}
      </Text>
    </TouchableHighlight>
  );
};

Item.defaultProps = {};

export default Item;
