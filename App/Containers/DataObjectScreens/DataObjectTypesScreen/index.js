import React, {useEffect, useContext} from 'react';

import {View, FlatList} from '../../../Components/Layout';

import {LIST_DATA_OBJECTS} from '../../../GraphQL/DataObjects';
import {useQuery} from '../../../Hooks';
import ContactContext from '../../../Containers/ContactsScreens/ContactDetailsScreen/ContactContext';
import DataObjectType from './Item';
import {moderateScale} from 'react-native-size-matters/extend';
import {Metrics} from '../../../Themes';

const DataObjectTypesScreen = (props) => {
  const {navigation} = props;
  const {contact, setInitialDataLoaded} = useContext(ContactContext);

  const {loading, initialLoaded, error, data, callQuery} = useQuery(
    LIST_DATA_OBJECTS,
    {},
  );

  console.tron.log('dataObject types', contact, loading, error, data);

  const reloadData = () => {
    console.tron.log('onRefresh Data Object Types reloadData');
    callQuery({});
  };

  useEffect(() => {
    navigation.setOptions({
      title: 'Data Objects',
    });
  }, [navigation]);

  useEffect(() => {
    if (initialLoaded) {
      setInitialDataLoaded('dataObject');
    }
  }, [initialLoaded]);

  return (
    <View isFlex>
      <FlatList
        contentContainerStyle={{
          paddingTop: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
        }}
        data={data.listDataObjects}
        keyExtractor={(item, index) => item.id || String(index)}
        loading={loading}
        onRefresh={() => reloadData()}
        renderItem={({item}) => (
          <DataObjectType contactId={contact.id} item={item} />
        )}
      />
    </View>
  );
};

DataObjectTypesScreen.defaultProps = {};

export default DataObjectTypesScreen;
