import React, {useState, useEffect, useRef} from 'react';

import {Content, BottomModal} from '../../../Components/Layout';
import {Button, IconButton} from '../../../Components/Buttons';
import {Heading} from '../../../Components/Text';

import FormInput from '../../Shared/Forms/FormInput';
import FormSelect from '../../Shared/Forms/FormSelect';
import FormTagsInput from '../../Shared/Forms/FormTagsInput';
import {useQuery, useWindowDimensions} from '../../../Hooks';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, ApplicationStyles} from '../../../Themes';
import {taskFiltersVar} from '../../../Cache';
import Loader from '../../../Components/Loader';

export const FILTER_HEIGHT = moderateScale(38, 0.3);
const FILTER_ICON_SIZE = moderateScale(18, 0.3);

const filterTypes = ['Terms', 'Tags'];

const ListFilter = (props) => {
  const {windowHeight} = useWindowDimensions();
  const {onApplyFilters} = props;
  const {loading, data: filters} = useQuery(ListFilter.name, {
    cacheFn: taskFiltersVar,
  });
  const [formFilters, setFormFilters] = useState({});
  const [filterType, setFilterType] = useState(
    formFilters.filterBy === 'tags' ? filterTypes[1] : filterTypes[0],
  );
  const modalRef = useRef();
  const tagsRef = useRef();

  useEffect(() => {
    setFormFilters(filters);
  }, [filters]);

  const onChangeFn = (type, value) => {
    const filters = {...formFilters, filterBy: type, [type]: value};

    setFormFilters(filters);
  };

  const onApplyFiltersFn = () => {
    console.tron.log('onApplyFiltersFn', formFilters);
    const isTerms = filterType === 'Terms';
    const filterAttr = isTerms ? 'terms' : 'tags';

    taskFiltersVar({...formFilters, ...(isTerms ? {tags: []} : {terms: ''})});

    let filterVal = formFilters[filterAttr];

    onApplyFilters(filterAttr, filterVal);

    modalRef.current.hide();
  };

  // const onResetAllFn = () => {
  //   const initialValues = {
  //     terms: '',
  //     tags: []
  //   }

  //   setFormFilters(initialValues)
  // }

  return (
    <>
      <IconButton
        containerStyle={{
          height: 'auto',
          // paddingHorizontal: 0
        }}
        icon="Filter"
        iconFill="#555555"
        iconSize={moderateScale(20, 0.1)}
        isAnimated
        onPress={() => modalRef.current.show()}
        style={{
          width: ApplicationStyles.floatingButtonLength,
          height: ApplicationStyles.floatingButtonLength,
          borderRadius: ApplicationStyles.floatingButtonLength,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FDFDFD',

          elevation: 3,
          shadowColor: Colors.black,
          shadowRadius: 5,
          shadowOpacity: 0.16,
          shadowOffset: {
            width: 0,
            height: 4,
          },
        }}
      />

      <BottomModal
        headerTitle={<Heading center>Filters</Heading>}
        ref={modalRef}
        // headerRight={
        //   <TouchableOpacity
        //     disabled={loading}
        //     onPress={onResetAllFn}
        //     style={{
        //       paddingHorizontal: Metrics.spaceHorizontal,
        //       height: moderateScale(44, 0.4),
        //       justifyContent: 'center'
        //     }}
        //   >
        //     {
        //       !loading
        //         ? <Text color="primary" weight='medium'>Reset all</Text>
        //         : <Loader color={Colors.purple} />
        //     }
        //   </TouchableOpacity>
        // }
      >
        {loading ? (
          <Loader />
        ) : (
          <Content
            isFlex={false}
            style={{
              // paddingTop: 20,
              paddingBottom: 12,
            }}>
            <FormSelect
              defaultValue={filterType}
              items={filterTypes}
              label="Filter By"
              onSelectPickerItem={(value) => setFilterType(value)}
            />

            {filterType === 'Terms' ? (
              <FormInput
                defaultValue={formFilters.terms}
                label="Search terms"
                onChangeText={(text) => onChangeFn('terms', text)}
                returnKeyType="done"
              />
            ) : (
              <FormTagsInput
                defaultTags={formFilters.tags}
                label="Search tags"
                onTagsUpdate={(value) => onChangeFn('tags', value)}
                ref={tagsRef}
              />
            )}

            {/* <RadioForm
                label='Include'
                initial={showTypes.findIndex(showType => showType.value === formFilters.show)}
                onPress={(value) => onChangeFn('show', value)}
                style={{
                  justifyContent: 'space-between'
                }}
                radio_props={showTypes}
                formHorizontal={true}
                labelHorizontal={true}
                animation={true}
                noBorder={true}
              /> */}

            <Button
              label="Apply Filters"
              onPress={onApplyFiltersFn}
              style={{
                marginTop: windowHeight * 0.26, //Metrics.hp(24)
              }}
            />
          </Content>
        )}
      </BottomModal>
    </>
  );
};

ListFilter.defaultProps = {
  onApplyFilters: () => {},
};

export default ListFilter;
