import React, {useState, useRef} from 'react';
import {TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import {View, Content, Modal} from '../../../Components/Layout';
import SvgIcon from '../../../Components/SvgIcon';
import {Switch} from '../../../Components/Forms';
import {Text} from '../../../Components/Text';
import {IconButton} from '../../../Components/Buttons';
import {Metrics, Colors} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {COMPLETE_TASK} from '../../../GraphQL/NotesTasks';
import {useQuery} from '../../../Hooks';
import moment from 'moment';
import HTML from '../../../Components/HTML';
import Alert from '../../../Containers/Shared/Alert';

const CONTAINER_VERTICAL_PADDING = moderateScale(16, 0.1);
const INNER_VERTICAL_PADDING = moderateScale(12, 0.1);
const NOTE_HEIGHT = moderateScale(Metrics.isIos ? 38 : 40, 0.4);
export const TASK_ITEM_HEIGHT = moderateScale(230, 0.2);

const Item = (props) => {
  const {item} = props;
  const taskSwitchRef = useRef();
  const modalRef = useRef();
  const completeTaskModalRef = useRef();
  const [isComplete, setIsComplete] = useState(!!item.completionDate);
  const [hasShowMore, setHasShowMore] = useState(false);
  const [isPromptTaskComplete, setIsPromptTaskComplete] = useState(false);

  const {callQuery} = useQuery(COMPLETE_TASK);

  const handleShow = async (shouldShow) => {
    if (shouldShow) {
      modalRef.current.show();
    } else {
      modalRef.current.hide();
    }
  };

  const onTaskComplete = () => {
    setIsComplete(true);
    callQuery({id: item.id}, 'completeTaskInput');
  };

  const setIsPromptTaskCompleteFn = (isVisible) => {
    setIsPromptTaskComplete(isVisible);
  };

  return (
    <View
      style={{
        backgroundColor: Colors.white,
        paddingVertical: CONTAINER_VERTICAL_PADDING,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        marginBottom: moderateScale(8, 0.3),
        height: TASK_ITEM_HEIGHT,
      }}>
      <Content isFlex={false} justifyBetween>
        <View alignCenter justifyBetween row>
          <View
            isFlex
            style={{
              paddingRight: moderateScale(12, 0.1),
            }}>
            <Text color="primary" weight="medium" xsmall>
              {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
            </Text>
            <Text
              color="darkerGray"
              numberOfLines={1}
              style={{marginTop: moderateScale(4, 0.1)}}
              xxxsmall>
              Assigned By:{' '}
              <Text color="darkerGray" weight="semibold" xxxsmall>
                {item.assignedBy}
              </Text>
            </Text>
          </View>

          <View alignCenter row>
            <Text color="darkerGray" xsmall>
              Done:{' '}
            </Text>
            <View
              style={{
                transform: [
                  {scaleX: moderateScale(0.8, 0.2)},
                  {scaleY: moderateScale(0.8, 0.2)},
                ],
              }}>
              <Switch
                defaultValue={isComplete}
                disabled={isComplete}
                onValueChange={(value) => {
                  if (value) {
                    completeTaskModalRef.current.show();
                  }
                }}
                ref={taskSwitchRef}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: Colors.border,
            marginVertical: INNER_VERTICAL_PADDING,
          }}
        />

        <View
          style={{
            paddingBottom: moderateScale(24, 0.1),
          }}>
          <View
            style={{
              height: NOTE_HEIGHT,
              overflow: 'hidden',
              // backgroundColor: 'red'
            }}>
            <View
              onLayout={({nativeEvent}) => {
                const {layout} = nativeEvent;
                const {height} = layout;

                if (height > NOTE_HEIGHT) {
                  setHasShowMore(true);
                }
              }}>
              <Text
                color="primary"
                ellipsizeMode="tail"
                numberOfLines={1}
                small
                style={{
                  marginTop: moderateScale(4, 0.3),
                }}
                weight="medium">
                {item.title}
              </Text>
              <HTML html={item.note} />
            </View>
          </View>

          {hasShowMore && (
            <TouchableOpacity
              onPress={() => handleShow(true)}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                position: 'absolute',
                right: 0,
                bottom: 0,
                // backgroundColor: 'red',
                paddingTop: moderateScale(20, 0.2),
              }}>
              <Text
                color="yellow"
                style={{
                  marginRight: moderateScale(4, 0.3),
                }}
                xxsmall>
                Show more
              </Text>
              <SvgIcon
                fill={Colors.yellow}
                height={moderateScale(10, 0.3)}
                name="DownChevron"
                width={moderateScale(10, 0.3)}
              />
            </TouchableOpacity>
          )}
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: Colors.border,
            marginVertical: INNER_VERTICAL_PADDING,
          }}
        />

        <Text color="small" style={{textAlign: 'right'}} xxxsmall>
          Assigned To:{' '}
          <Text color="small" weight="semibold" xxxsmall>
            {item.assignedTo}
          </Text>
        </Text>
      </Content>

      {item.tags && item.tags.length > 0 && (
        <View isFlex justifyEnd>
          <View>
            <ScrollView
              bounces={false}
              contentContainerStyle={{
                paddingHorizontal: Metrics.spaceHorizontal,
              }}
              horizontal={true}>
              <View alignCenter row>
                {item.tags.map((tag, index) => {
                  return (
                    <View
                      key={index}
                      style={{
                        paddingHorizontal: moderateScale(4, 0.3),
                        paddingVertical: moderateScale(4, 0.3),
                        borderRadius: moderateScale(4, 0.3),
                        borderWidth: 0.1,
                        borderColor: Colors.darkerGray,
                        backgroundColor: Colors.lighterGray,
                        marginRight: moderateScale(8, 0.3),
                        marginBottom: moderateScale(4, 0.3),
                      }}>
                      <Text color="darkerGray" xsmall>
                        #{tag}
                      </Text>
                    </View>
                  );
                })}
              </View>
            </ScrollView>
          </View>
        </View>
      )}

      <Modal alignCenter justifyCenter ref={modalRef}>
        <View
          style={{
            backgroundColor: '#F2F2F2',
            borderRadius: 14,
            width: moderateScale(268, 0.3),
            paddingTop: moderateScale(8, 0.3),
          }}>
          <View alignCenter justifyBetween row>
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                alignItems: 'center',
              }}>
              <Text center weight="semibold" xxxlarge>
                Notes
              </Text>
            </View>

            <IconButton
              containerStyle={{
                alignSelf: 'flex-end',
                alignItems: 'flex-end',
              }}
              icon="Close"
              iconFill={Colors.darkerGray}
              iconSize={moderateScale(16, 0.1)}
              isAnimated
              onPress={() => handleShow(false)}
            />
          </View>

          <View
            style={{
              paddingHorizontal: Metrics.spaceHorizontal,
              paddingTop: CONTAINER_VERTICAL_PADDING,
              maxHeight: Metrics.hp(74),
            }}>
            <ScrollView
              contentContainerStyle={{
                paddingTop: moderateScale(10, 0.1),
              }}>
              <HTML html={item.note} />
            </ScrollView>
          </View>
        </View>
      </Modal>

      <Alert
        ref={completeTaskModalRef}
        style={{
          height: moderateScale(178),
          marginBottom: moderateScale(220),
        }}>
        <>
          <View
            isFlex
            justifyCenter
            style={{
              paddingHorizontal: moderateScale(20),
              paddingVertical: moderateScale(20),
            }}>
            <Text
              center
              style={{
                marginBottom: moderateScale(16),
              }}
              weight="medium"
              xlarge>
              Complete Task?
            </Text>
            <Text center small>
              You won't be able to undo
            </Text>
            <Text center small>
              this action
            </Text>
          </View>

          <View row>
            <TouchableOpacity
              onPress={() => {
                taskSwitchRef.current.toggleSwitch();
                completeTaskModalRef.current.hide();
              }}
              style={styles.button}>
              <Text
                center
                style={{
                  color: '#AAAAAA',
                }}
                xlarge>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                completeTaskModalRef.current.hide();
                onTaskComplete();
              }}
              style={styles.button}>
              <Text
                center
                style={{
                  color: '#007AFF',
                }}
                xlarge>
                Yes
              </Text>
            </TouchableOpacity>
          </View>
        </>
      </Alert>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    height: moderateScale(44),
    justifyContent: 'center',
  },
});

Item.defaultProps = {};

export default Item;
