import React, {useState, useLayoutEffect} from 'react';
import {TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import {View} from '../../../Components/Layout';

import {Text} from '../../../Components/Text';
import SvgIcon from '../../../Components/SvgIcon';

import {useQuery} from '../../../Hooks';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, Metrics} from '../../../Themes';
import {taskFiltersVar} from '../../../Cache';

import {
  getPropertiesWithValues,
  convertArrayToObject,
} from '../../../Utils/array';

const FilterTabs = (props) => {
  const {onRemoveTab} = props;
  const {loading, data: filters} = useQuery(FilterTabs.name, {
    cacheFn: taskFiltersVar,
  });
  const [tabs, setTabs] = useState([]);

  useLayoutEffect(() => {
    const filterTabs = getPropertiesWithValues(filters, [
      'All',
      'terms',
      'tags',
    ]);

    setTabs(filterTabs);
  }, [filters]);

  const removeTab = async (tabId, filterBy) => {
    const newTabs = tabs.filter((oldTab) => oldTab.id !== tabId);
    setTabs(newTabs);

    const initialValues = {
      terms: '',
      tags: [],
    };

    const updatedTaskFilters = convertArrayToObject(newTabs, initialValues);

    taskFiltersVar({...updatedTaskFilters, filterBy});

    onRemoveTab(filterBy, updatedTaskFilters[filterBy]);
  };

  if (!tabs.length) {
    return (
      <View
        style={{
          height: moderateScale(16, 0.3),
        }}
      />
    );
  }

  return (
    <>
      <View
        style={{
          height: moderateScale(64, 0.3),
        }}>
        <ScrollView
          contentContainerStyle={{
            paddingHorizontal: Metrics.spaceHorizontal,
          }}
          horizontal={true}>
          <View style={styles.tagsContainer}>
            {tabs.map((filter) => {
              return (
                <TouchableOpacity
                  key={filter.id}
                  onPress={() => {
                    const {id, key: filterBy} = filter;
                    removeTab(id, filterBy);
                  }}
                  style={[
                    styles.tagContainer,
                    {
                      elevation: 1,
                      shadowColor: Colors.black,
                      shadowRadius: 2,
                      shadowOpacity: 0.2,
                      shadowOffset: {
                        width: 2,
                        height: 2,
                      },
                    },
                  ]}>
                  <View style={{marginRight: moderateScale(8, 0.3)}}>
                    <Text style={{color: '#555555'}} weight="medium" xsmall>
                      {filter.value}
                    </Text>
                  </View>
                  <SvgIcon
                    fill={Colors.primary}
                    height={moderateScale(9, 0.3)}
                    name="Close"
                    width={moderateScale(9, 0.3)}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  tagContainer: {
    alignItems: 'center',
    backgroundColor: '#FDFDFD',
    borderRadius: moderateScale(20, 0.3),
    flexDirection: 'row',
    height: moderateScale(32, 0.3),
    marginRight: moderateScale(8, 0.3),
    paddingHorizontal: moderateScale(16, 0.3),
  },
  tagsContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
});

FilterTabs.defaultProps = {
  onRemoveTab: () => {},
};

export default FilterTabs;
