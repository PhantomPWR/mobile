import React, {useState, useEffect} from 'react';

import {useQuery} from '../../../Hooks';
import {LIST_USERS} from '../../../GraphQL/Users';
import {FloatingLabelInfiniteSelect} from '../../../Components/Forms';
import {moderateScale} from 'react-native-size-matters/extend';

const SELECT_ITEMS_LIMIT = 10;

const FormAssignToSelect = (props) => {
  const [users, setUsers] = useState([]);

  const {loading, initialLoaded, error, data, callQuery} = useQuery(
    LIST_USERS,
    null,
    {skipInitialize: true},
  );

  // console.tron.log("LIST_USERS", loading, initialLoaded, error, data)

  useEffect(() => {
    const response = (data.listUsers && data.listUsers.items) || [];
    const transformedData = response.map((d) => d.email);

    setUsers(transformedData);
  }, [data]);

  return (
    <FloatingLabelInfiniteSelect
      // disabled={initialLoaded}
      initialLoaded={initialLoaded}
      inputPaddingTop={moderateScale(8, 0.3)}
      listProps={{
        data: users,
        getData: (page, limit) => {
          callQuery({page: page - 1, limit});
        },

        // ref: listRef}
        // contentContainerStyle: {
        //   paddingVertical: moderateScale(8, 0.3)
        // }}
        keyExtractor: (item) => item,
        // keyExtractor: (item, index) => String(index)}
        loading,
        pageItemsLength: SELECT_ITEMS_LIMIT,
      }}
      style={{
        justifyContent: 'space-between',
        // paddingRight: Metrics.spaceHorizontal
      }}
      {...props}
    />
  );
};

export default FormAssignToSelect;
