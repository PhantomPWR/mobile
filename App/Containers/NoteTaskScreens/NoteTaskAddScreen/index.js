import React from 'react';
import {ScrollView} from 'react-native';
import ScreenContainer from '../../Shared/ScreenContainer';
import {View} from '../../../Components/Layout';

import NoteForms from './NoteForms';
import TaskForms from './TaskForms';
import Const from '../../../Constants';

export const DEFAULT_NOTE_TAGS = [
  'general',
  'call note',
  'alert',
  'update',
  'comment',
];

export const DEFAULT_TASK_TAGS = ['task'];

export const filterNoteTags = (tags) => {
  return tags.filter((v) => !DEFAULT_NOTE_TAGS.includes(v.toLowerCase()));
};

export const filterTaskTags = (tags) => {
  return tags.filter((v) => !DEFAULT_TASK_TAGS.includes(v.toLowerCase()));
};

const NoteTaskAddScreen = (props) => {
  const {params = {}} = props.route;
  const isEdit = params.isEdit;
  const noteInitValues = {
    ...(isEdit ? {id: params.id} : {contactId: params.contactId}),
    type: params.type || Const.ACTION_TYPES[0],
    title: params.title || '',
    note: params.note || '',
    tags: params.tags ? filterNoteTags(params.tags) : [],
  };
  const taskInitValues = {
    ...(isEdit
      ? {id: params.id}
      : {userId: params.userId, contactId: params.contactId}),
    title: params.title || '',
    note: params.note || '',
    userId: params.userId,
    priority: params.priority || 2,
    tags: params.tags ? filterTaskTags(params.tags) : [],
  };
  // const [isNote, setIsNote] = useState(true)
  const isTask = params.isTask;

  console.tron.log('isEdit', isEdit);

  // const onTypeChange = (value) => {
  //   setIsNote(!value)
  // }

  return (
    <ScreenContainer
      headerTitle={`${!isEdit ? 'Add' : 'Edit'} ${!isTask ? 'Note' : 'Task'}`}>
      <ScrollView
        bounces={false}
        contentContainerStyle={{
          minHeight: '100%',
          // paddingTop: moderateScale(16, 0.3)
        }}
        keyboardShouldPersistTaps="always">
        {/* <PanelHeader
          label='Is this a task?'
        >
          <Switch
            onValueChange={onTypeChange}
          />
        </PanelHeader> */}

        <View isFlex>
          {!isTask ? (
            <NoteForms
              initValues={noteInitValues}
              isEdit={isEdit}
              onSuccess={params.onSuccess}
            />
          ) : (
            <TaskForms
              initValues={taskInitValues}
              isEdit={isEdit}
              onSuccess={params.onSuccess}
            />
          )}
        </View>
      </ScrollView>
    </ScreenContainer>
  );
};

export default NoteTaskAddScreen;
