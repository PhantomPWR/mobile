import React, {useEffect, useRef} from 'react';
import {Content, View} from '../../../Components/Layout';
import {ErrorMessage} from '../../../Components/Text';
import FormRichInput from '../../../Components/Forms/FormRichInput';
import FormInput from '../../Shared/Forms/FormInput';
import FormSelect from '../../Shared/Forms/FormSelect';
import FormTagsInput from '../../Shared/Forms/FormTagsInput';
import SubmitButton from '../../Shared/Forms/SubmitButton';

import {createNoteValidationSchema} from '../../../Utils/validations';
import {Formik} from 'formik';
import {CREATE_NOTE, UPDATE_NOTE} from '../../../GraphQL/NotesTasks';
import {useQuery} from '../../../Hooks';
import {Colors, Metrics} from '../../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';
import {useNavigation} from '@react-navigation/native';
import Const from '../../../Constants';
import {filterNoteTags} from '.';

const NoteForms = (props) => {
  const {initValues, isEdit, onSuccess} = props;
  const navigation = useNavigation();
  const tagsRef = useRef();
  const noteRef = useRef();

  const {
    loading: createLoading,
    error: createError,
    data: createData,
    callQuery,
  } = useQuery(CREATE_NOTE);
  const {
    loading: updateLoading,
    error: updateError,
    data: updateData,
    callQuery: callUpdateQuery,
  } = useQuery(UPDATE_NOTE);

  const loading = createLoading || updateLoading;
  const error = createError || updateError;

  useEffect(() => {
    if (
      (!createLoading && !createError && createData.createNote) ||
      (!updateLoading && !updateError && updateData.updateNote)
    ) {
      console.tron.log('SUCCESS SAVE Note!!');
      onSuccess();
      navigation.goBack();
    }
  }, [createLoading, updateLoading]);

  return (
    <Formik
      initialValues={initValues}
      onSubmit={(values) => {
        if (!isEdit) {
          callQuery(values, 'createNoteInput');
        } else {
          callUpdateQuery(values, 'updateNoteInput');
        }
      }}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={createNoteValidationSchema}>
      {({handleSubmit, handleChange, values, errors, setFieldValue}) => (
        <>
          <Content
            isFlex={false}
            style={{
              backgroundColor: Colors.white,
            }}>
            <FormSelect
              defaultValue={values.type}
              items={Const.ACTION_TYPES}
              label="Action Type"
              onSelectPickerItem={(value) => setFieldValue('type', value)}
            />

            <FormInput
              // autoFocus
              autoCapitalize="words"
              defaultValue={values.title}
              error={errors.title}
              label="Title"
              onChangeText={handleChange('title')}
              onSubmitEditing={() => {
                console.tron.log('tagsRef', tagsRef);
                tagsRef.current.focus();
              }}
              returnKeyType="next"
            />

            <FormTagsInput
              autoCapitalize="words"
              defaultTags={filterNoteTags(values.tags)}
              error={errors.tags}
              label="Tags"
              onTagsUpdate={(value) =>
                setFieldValue('tags', filterNoteTags(value))
              }
              ref={tagsRef}
            />

            <FormRichInput
              defaultContentHTML={values.note}
              label="Type your notes here..."
              onChange={handleChange('note')}
              ref={noteRef}
            />
          </Content>

          <SubmitButton
            isLoading={loading}
            onPress={handleSubmit}
            title="SAVE"
          />

          <View
            isFlex
            justifyEnd
            style={{
              paddingVertical: moderateScale(20, 0.3),
              paddingHorizontal: Metrics.spaceHorizontal,
              // backgroundColor: Colors.background
            }}>
            <ErrorMessage center>{error}</ErrorMessage>
          </View>
        </>
      )}
    </Formik>
  );
};

export default NoteForms;
