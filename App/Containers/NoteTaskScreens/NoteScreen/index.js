import React, {useEffect, useContext, useRef} from 'react';

import {View, InfiniteScrollList} from '../../../Components/Layout';
import {IconButton} from '../../../Components/Buttons';
import {LIST_NOTES} from '../../../GraphQL/NotesTasks';
import {useQuery} from '../../../Hooks';
import ContactContext from '../../../Containers/ContactsScreens/ContactDetailsScreen/ContactContext';
import FilterTabs from './FilterTabs';
import ListFilter from './ListFilter';
import NoteItem, {NOTE_ITEM_HEIGHT} from './Item';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, Metrics, ApplicationStyles} from '../../../Themes';
import {noteFiltersVar} from '../../../Cache';

const getFilterVal = (filterBy, filter) => {
  let filterVal = filter;

  if (filterBy === 'tags') {
    filterVal = filterVal.join(',');
  }

  return filterVal;
};

const NoteScreen = (props) => {
  const {navigation} = props;
  const listRef = useRef();
  const {contact, setInitialDataLoaded} = useContext(ContactContext);

  const {loading, initialLoaded, error, data, callQuery} = useQuery(
    LIST_NOTES,
    null,
    {skipInitialize: true},
  );

  console.tron.log('notes', contact, loading, error, data);

  const reloadData = () => {
    console.tron.log('onRefresh Notes reloadData');
    listRef.current.resetPage();
    callQuery({contactId: contact.id, page: 0, limit: 10});
  };

  // useEffect(() => {
  //   if (data.listNotes) {
  //     navigation.setOptions({
  //       title: `Notes (${data.listNotes.total})`,
  //     });
  //   }
  // }, [data.listNotes && data.listNotes.total]);

  useEffect(() => {
    if (initialLoaded) {
      setInitialDataLoaded('note');
    }
  }, [initialLoaded]);

  return (
    <View isFlex>
      <InfiniteScrollList
        contentContainerStyle={
          {
            // paddingTop: moderateScale(8, 0.3),
            // paddingHorizontal: Metrics.spaceHorizontal,
            // paddingBottom: Metrics.spaceHorizontal * 2 + ApplicationStyles.floatingButtonLength + Metrics.spaceHorizontal + ApplicationStyles.floatingButtonLength
          }
        }
        data={data.listNotes && data.listNotes.items}
        getData={async (page, limit) => {
          const formFilters = await noteFiltersVar();
          const filterBy = formFilters.filterBy;
          const filter = formFilters[formFilters.filterBy];

          const filterVal = getFilterVal(filterBy, filter);

          callQuery({
            contactId: contact.id,
            page: page - 1,
            limit,
            filterBy,
            filter: filterVal,
          });
        }}
        getItemLayout={(data, index) => ({
          length: NOTE_ITEM_HEIGHT,
          offset: NOTE_ITEM_HEIGHT * index,
          index,
        })}
        keyExtractor={(item, index) => item.id || String(index)}
        // keyExtractor={(item, index) => String(index)}
        ListHeaderComponent={() => (
          <FilterTabs
            onRemoveTab={(filterBy, filter) => {
              listRef.current.resetPage();

              const filterVal = getFilterVal(filterBy, filter);

              callQuery({
                contactId: contact.id,
                page: 0,
                limit: 10,
                filterBy,
                filter: filterVal,
              });
            }}
          />
        )}
        loading={loading}
        onLoadData={(listData) => {
          navigation.setOptions({
            title: `Notes (${listData.length})`,
          });
        }}
        pageItemsLength={10}
        ref={listRef}
        renderItem={({item}) => <NoteItem item={item} onEdit={reloadData} />}
      />

      <View
        alignCenter
        pointerEvents="box-none"
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          paddingVertical: moderateScale(16, 0.3),
          paddingHorizontal: Metrics.spaceHorizontal,
          // backgroundColor: 'red'
        }}>
        <ListFilter
          onApplyFilters={(filterBy, filter) => {
            listRef.current.resetPage();
            const filterVal = getFilterVal(filterBy, filter);

            callQuery({
              contactId: contact.id,
              page: 0,
              limit: 10,
              filterBy,
              filter: filterVal,
            });
          }}
        />

        <IconButton
          containerStyle={{
            height: 'auto',
            marginTop: Metrics.spaceHorizontal,
            // paddingHorizontal: 0
          }}
          icon="Plus"
          iconFill={Colors.white}
          iconSize={moderateScale(16, 0.1)}
          isAnimated
          onPress={() =>
            navigation.navigate('NoteTaskAdd', {
              contactId: contact.id,
              isTask: false,
              onSuccess: reloadData,
            })
          }
          style={{
            width: ApplicationStyles.floatingButtonLength,
            height: ApplicationStyles.floatingButtonLength,
            borderRadius: ApplicationStyles.floatingButtonLength,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.black,

            elevation: 3,
            shadowColor: Colors.black,
            shadowRadius: 5,
            shadowOpacity: 0.16,
            shadowOffset: {
              width: 0,
              height: 4,
            },
          }}
        />
      </View>
    </View>
  );
};

NoteScreen.defaultProps = {};

export default NoteScreen;
