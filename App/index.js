import './Config';
import './Containers/Shared/ErrorHandler';
import React from 'react';
import RootContainer from './Containers/RootContainer';
import AWSAppSyncClient from 'aws-appsync';
// import AppSyncConfig from './aws-exports'
import {ApolloProvider} from 'react-apollo';

// import { CREATE_NOTE } from './GraphQL/NotesTasks'
// import { Rehydrated } from 'aws-appsync-react' // this needs to also be installed when working with React

const AppSyncConfig = {
  graphqlEndpoint:
    'https://bqpayrm6ubah7f2k7sxvh2tk7e.appsync-api.us-east-1.amazonaws.com/graphql',
  region: 'us-east-1',
  authenticationType: 'API_KEY',
  apiKey: 'da2-o75xqa3rzbh73oj42xecyuhf3a',
};

export const client = new AWSAppSyncClient({
  url: AppSyncConfig.graphqlEndpoint,
  region: AppSyncConfig.region,
  auth: {
    type: AppSyncConfig.authenticationType,
    apiKey: AppSyncConfig.apiKey,
    // jwtToken: async () => token, // Required when you use Cognito UserPools OR OpenID Connect. token object is obtained previously
  },
});

// client.hydrated().then(function (client) {
//   // Now run a mutation
//   const vari = {
//     createNoteInput: {
//       appName: "sx529",
//       apiKey: "qpj91vodlgbxf3cekrnw7zih4ys0825t",
//       contactId: 9,
//       type: "General",
//       title: "Test 3 Title Note",
//       note: "",
//       tags: []
//     }
//   }

//   console.tron.log('(Mutate): add contact', vari);

//   client.mutate({
//     mutation: CREATE_NOTE,
//     variables:vari,
//     fetchPolicy: 'no-cache'
//   })
//     .then(function logData(data) {
//         console.tron.log('(Mutate): Inserting Data ----------->', data);
//     })
//     .catch(err => console.tron.log("ERROR ON MUTATION", err))
// })

const App = () => {
  return (
    <ApolloProvider client={client}>
      {/* <Rehydrated> */}
      <RootContainer />
      {/* </Rehydrated> */}
    </ApolloProvider>
  );
};

// allow reactotron overlay for fast design in dev mode
export default App;
