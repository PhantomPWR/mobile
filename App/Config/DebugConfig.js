export default {
  useFixtures: true,
  yellowBox: __DEV__,
  useReactotron: __DEV__,
  enableSentry: !__DEV__,
};
