import {NativeModules} from 'react-native';
import Config from '../Config/DebugConfig';
import Reactotron from 'reactotron-react-native';
import ReactotronFlipper from 'reactotron-react-native/dist/flipper';
import AsyncStorage from '@react-native-community/async-storage';

let scriptHostname;
if (Config.useReactotron) {
  const scriptURL = NativeModules.SourceCode.scriptURL;
  scriptHostname = scriptURL.split('://')[1].split(':')[0];
}

const options = Config.useReactotron
  ? {host: scriptHostname, createSocket: (path) => new ReactotronFlipper(path)}
  : undefined;

const reactotron = Reactotron.setAsyncStorageHandler(AsyncStorage)
  .configure(options)
  .useReactNative();

if (Config.useReactotron) {
  // https://github.com/infinitered/reactotron for more options!

  reactotron.connect();

  // Let's clear Reactotron on every time we load the app
  reactotron.clear();

  // Totally hacky, but this allows you to not both importing reactotron-react-native
  // on every file. This is just DEV mode, so no big deal.
}
export default reactotron;
console.tron = reactotron;
