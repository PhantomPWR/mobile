import React from 'react';
import {View} from '../../Components/Layout';
import {KeyboardAvoidingView, Platform} from 'react-native';
import {ApplicationStyles} from '../../Themes';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useHeaderHeight} from '@react-navigation/stack';

const Container = (props) => {
  const {style, hasBottomInset, ...allProps} = props;
  const insets = useSafeAreaInsets();
  const headerHeight = useHeaderHeight();

  const bottomInset = hasBottomInset ? insets.bottom : 0;
  const offsetAttrs = hasBottomInset && {
    keyboardVerticalOffset: headerHeight - bottomInset,
  };

  return (
    <KeyboardAvoidingView
      {...offsetAttrs}
      // keyboardVerticalOffset={headerHeight - bottomInset}
      // keyboardVerticalOffset={bottomInset}
      behavior={Platform.select({
        ios: 'padding',
      })}
      style={[ApplicationStyles.screenContainer, style]}>
      <View
        isFlex
        style={{
          paddingBottom: bottomInset,
        }}
        {...allProps}
      />
    </KeyboardAvoidingView>
  );
};

export default Container;
