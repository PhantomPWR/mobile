import React from 'react';
import {Animated} from 'react-native';
import {View} from '../../Components/Layout';

export function withAnimated(
  WrappedComponent: React.ComponentType<any>,
): ComponentType {
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component';

  class WithAnimated extends React.Component {
    static displayName = `WithAnimated(${displayName})`;

    render(): React.ReactNode {
      return <WrappedComponent {...this.props} />;
    }
  }

  return Animated.createAnimatedComponent(WithAnimated);
}

const AnimatedViewContainer = Animated.createAnimatedComponent(
  withAnimated(View),
);

const AnimatedView = (props) => {
  return <AnimatedViewContainer {...props} />;
};

export default AnimatedView;
