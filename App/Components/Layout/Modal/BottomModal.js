import React, {forwardRef, useRef, useImperativeHandle} from 'react';
import Modal from '.';
import View from '../View';
import {Metrics, Colors} from '../../../Themes';
import {IconButton} from '../../Buttons';
import {moderateScale} from 'react-native-size-matters/extend';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const BottomModal = forwardRef((props, ref) => {
  const {
    children,
    headerTitle,
    headerRight,
    containerStyle,
    ...allProps
  } = props;
  const insets = useSafeAreaInsets();
  const modalRef = useRef();

  const bottomInset = insets.bottom;

  useImperativeHandle(ref, () => modalRef.current, []);

  return (
    <Modal justifyEnd ref={modalRef} {...allProps}>
      <View
        // alignCenter
        style={[
          {
            borderTopLeftRadius: moderateScale(24, 0.3),
            borderTopRightRadius: moderateScale(24, 0.3),
            backgroundColor: Colors.white,
            paddingTop: Metrics.spaceHorizontal,
            paddingBottom: Metrics.spaceHorizontal + bottomInset,
            width: '100%',
          },
          containerStyle,
        ]}>
        <View
          alignCenter
          justifyBetween
          row
          style={{
            marginBottom: moderateScale(12, 0.3),
          }}>
          {headerTitle && (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                alignItems: 'center',
              }}>
              {headerTitle}
            </View>
          )}

          <IconButton
            containerStyle={{
              alignSelf: 'flex-end',
              alignItems: 'flex-end',
            }}
            icon="Close"
            iconFill={Colors.darkerGray}
            iconSize={moderateScale(16, 0.1)}
            isAnimated
            onPress={() => modalRef.current.hide()}
          />

          {headerRight}
        </View>

        {children}
      </View>
    </Modal>
  );
});

export default BottomModal;
