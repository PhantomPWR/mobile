import React, {
  forwardRef,
  useState,
  useEffect,
  useImperativeHandle,
} from 'react';
import {
  Modal as NativeModal,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import View from '../View';

const SUPPORTED_ORIENTATIONS = [
  'portrait',
  'landscape-left',
  'landscape-right',
];

let Modal = (props, ref) => {
  const {
    children,
    style,
    animationType,
    disableBackdropPress,
    isVisible,
    ...allProps
  } = props;
  const [isModalVisible, setIsModalVisible] = useState(isVisible);

  useEffect(() => {
    if (isVisible !== isModalVisible) {
      setIsModalVisible(isVisible);
    }
  }, [isVisible]);

  useImperativeHandle(
    ref,
    () => {
      return {
        show: () => setIsModalVisible(true),
        hide: () => setIsModalVisible(false),
      };
    },
    [],
  );

  return (
    <NativeModal
      animationType={animationType}
      onRequestClose={() => console.log('closed')}
      supportedOrientations={SUPPORTED_ORIENTATIONS}
      transparent
      visible={isModalVisible}>
      <View
        isFlex
        style={[
          {
            backgroundColor: 'rgba(0,0,0,0.4)',
          },
          style,
        ]}
        {...allProps}>
        <TouchableWithoutFeedback
          disabled={disableBackdropPress}
          onPress={() => setIsModalVisible(false)}>
          <View style={StyleSheet.absoluteFillObject} />
        </TouchableWithoutFeedback>
        {children}
      </View>
    </NativeModal>
  );
};

Modal = forwardRef(Modal);

Modal.defaultProps = {
  animationType: 'slide',
  disableBackdropPress: false,
  isVisible: false,
};

export default Modal;
