import React, {Component} from 'react';
import {View, FlatList, RefreshControl} from 'react-native';
import {Label} from '../../../Components/Text';
import {Colors} from '../../../Themes';
import Loader from '../../../Components/Loader';
import {moderateScale} from 'react-native-size-matters/extend';

class InfiniteScrollList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.data,
    };

    this.loadedPage = 1;
    this.loadingMore = false;
    this.preventLoadMoreData = true;
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data && Array.isArray(this.props.data)) {
      let newData =
        this.loadedPage === 1
          ? Array.from(this.props.data)
          : [...this.state.data, ...this.props.data];

      this.setState(
        {
          data: newData,
        },
        () => {
          this.props.onLoadData(newData, this.loadedPage);

          this.loadingMore = false;
          this.preventLoadMoreData = true;
        },
      );
    }
  }

  getData = (page = 1) => {
    this.props.getData(page, this.props.pageItemsLength);
  };

  resetPage = () => {
    this.listRef.scrollToOffset({y: 0, animated: false});
    this.loadedPage = 1;
  };

  _handleRefresh = () => {
    this.loadedPage = 1;
    this.preventLoadMoreData = true;

    this.getData(this.loadedPage);
  };

  _handleLoadMore = () => {
    if (
      this.loadingMore !== true &&
      this.preventLoadMoreData !== true &&
      this.state.data.length !== 0 &&
      this.props.data.length === this.props.pageItemsLength
    ) {
      this.loadingMore = true;
      ++this.loadedPage;

      this.getData(this.loadedPage);
    }

    console.tron.log(
      '_handleLoadMore - current data length: ' +
        this.state.data.length +
        ' - new data length: ' +
        this.props.data.length +
        ' - page: ' +
        this.loadedPage +
        ' - this.loadingMore: ' +
        this.loadingMore,
    );
  };

  handleOnScrollBeginDrag = () => {
    console.tron.log('handleOnScrollBeginDrag');
    this.preventLoadMoreData = false;
    this.props.onScrollBeginDrag();
  };

  _renderFooter = () => {
    if (
      this.state.data.length !== 0 &&
      this.props.data.length === this.props.pageItemsLength
    ) {
      return (
        <View
          style={{
            paddingVertical: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loader size="large" />
        </View>
      );
    }

    if (!this.props.loading && this.state.data.length === 0) {
      return (
        <Label center darkerGray style={{marginTop: moderateScale(8)}}>
          No results found
        </Label>
      );
    }

    return null;
  };

  render() {
    const {loading, ...allProps} = this.props;

    return (
      <FlatList
        initialNumToRender={this.props.pageItemsLength}
        ListFooterComponent={this._renderFooter}
        onEndReached={this._handleLoadMore}
        onEndReachedThreshold={0.5}
        ref={(ref) => (this.listRef = ref)}
        refreshControl={
          <RefreshControl
            colors={[Colors.darkerGray]}
            onRefresh={this._handleRefresh}
            refreshing={loading && this.preventLoadMoreData}
            tintColor={Colors.darkerGray}
          />
        }
        {...allProps}
        data={this.state.data}
        onScrollBeginDrag={this.handleOnScrollBeginDrag}
      />
    );
  }
}

InfiniteScrollList.defaultProps = {
  data: [],
  pageItemsLength: 10,
  onScrollBeginDrag: () => {},
  onLoadData: () => {},
};

export default InfiniteScrollList;
