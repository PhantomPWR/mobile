import React from 'react';
import {FlatList as NativeFlatList, RefreshControl} from 'react-native';

import {Colors} from '../../../Themes';
import {Label} from '../../../Components/Text';
import {moderateScale} from 'react-native-size-matters/extend';

const FlatList = (props) => {
  const {data, loading, onRefresh, ...allProps} = props;

  const items = data || [];

  return (
    <NativeFlatList
      data={items}
      keyExtractor={(item, index) => item.id || index}
      ListFooterComponent={() =>
        loading
          ? null
          : items.length === 0 && (
              <Label center darkerGray style={{marginTop: moderateScale(8)}}>
                No results found
              </Label>
            )
      }
      refreshControl={
        <RefreshControl
          colors={[Colors.darkerGray]}
          onRefresh={onRefresh}
          refreshing={loading}
          tintColor={Colors.darkerGray}
        />
      }
      {...allProps}
    />
  );
};

export default FlatList;
