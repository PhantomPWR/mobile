import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from 'react';
import {Animated} from 'react-native';
import View from './View';

let ToggleView = (props, ref) => {
  const {
    toggle,
    style,
    listTop,
    children,
    isAnimated,
    animatedIsVisible,
    defaultValue,
    ...allProps
  } = props;
  const [isVisible, setIsVisible] = useState(defaultValue);

  const onToggle = () => {
    setIsVisible((prevState) => !prevState);
  };

  useEffect(() => {
    if (animatedIsVisible) {
      Animated.timing(animatedIsVisible, {
        useNativeDriver: false,
        toValue: isVisible ? 1 : 0,
        duration: 200,
      }).start();
    }
  }, [isVisible]);

  useImperativeHandle(
    ref,
    () => {
      return {
        toggle: onToggle,
      };
    },
    [],
  );

  const ViewContainer = isAnimated ? Animated.View : View;

  return (
    <>
      <ViewContainer
        style={[
          {
            position: 'absolute',
            top: listTop,
            left: 0,
            right: 0,
          },
          style,
          !isVisible && {
            position: 'relative',
            display: 'none',
          },
        ]}
        {...allProps}>
        {children}
      </ViewContainer>

      {toggle && toggle(onToggle)}
    </>
  );
};

ToggleView = forwardRef(ToggleView);

ToggleView.defaultProps = {
  defaultValue: false,
  listTop: '100%',
};

export default ToggleView;
