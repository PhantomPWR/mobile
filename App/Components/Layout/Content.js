import React from 'react';
import View from './View';
import {Metrics} from '../../Themes';

const Content = (props) => {
  const {style, ...allProps} = props;
  return (
    <View
      isFlex
      style={[
        {
          paddingHorizontal: props.largeSpace
            ? Metrics.spaceHorizontalLg
            : Metrics.spaceHorizontal,
        },
        style,
      ]}
      {...allProps}
    />
  );
};

export default Content;
