import React, {useState, useCallback} from 'react';
import {Keyboard, TouchableOpacity, StyleSheet} from 'react-native';
import FormInput from './FormInput';
import {Label} from '../Text';
import {Colors, ApplicationStyles} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height, 0.3);

const PasswordInput = React.forwardRef((props, ref) => {
  const [isSecure, setIsSecure] = useState(true);
  const [isFocused, setIsFocused] = useState(false);
  const [password, setPassword] = useState(false);

  const {
    children,
    onChangeText,
    onSubmitEditing,
    onFocus,
    onBlur,
    ...allProps
  } = props;

  const onShowPassword = useCallback(() => {
    setIsSecure(!isSecure);
  }, [isSecure]);

  const onChangeTextFn = (text) => {
    setPassword(text);

    onChangeText && onChangeText(text);
  };

  const onFocusFn = () => {
    setIsFocused(true);

    onFocus && onFocus();
  };

  const onBlurFn = () => {
    setIsFocused(false);

    onBlur && onBlur();
  };

  const showButton = isFocused || !!password;

  return (
    <FormInput
      blurOnSubmit={false}
      noClear
      onBlur={onBlurFn} // fix for https://github.com/facebook/react-native/issues/21911
      onChangeText={onChangeTextFn}
      onFocus={onFocusFn}
      onSubmitEditing={() => {
        Keyboard.dismiss(); // fix for https://github.com/facebook/react-native/issues/21911
        onSubmitEditing();
      }}
      ref={ref}
      secureTextEntry={isSecure}
      textContentType="password"
      {...allProps}>
      {children}

      {showButton && (
        <TouchableOpacity
          onPress={onShowPassword}
          style={styles.toggleContainer}>
          <Label style={styles.toggleLabel}>{isSecure ? 'Show' : 'Hide'}</Label>
        </TouchableOpacity>
      )}
    </FormInput>
  );
});

const styles = StyleSheet.create({
  toggleContainer: {
    alignItems: 'center',
    height: INPUT_HEIGHT,
    justifyContent: 'center',
    paddingHorizontal: moderateScale(18, 0.3),
    position: 'absolute',
    right: 0,
    top: 0,
  },
  toggleLabel: {
    color: Colors.yellow,
    fontSize: moderateScale(12, 0.3),
  },
});

export default PasswordInput;
