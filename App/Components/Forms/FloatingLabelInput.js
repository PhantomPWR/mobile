import React, {forwardRef, useState, useEffect, useRef} from 'react';
import {Animated, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, ApplicationStyles, Metrics} from '../../Themes';
import SvgIcon from '../../Components/SvgIcon';
import {View} from '../Layout';
import {ErrorMessage} from '../../Components/Text';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height, 0.3);
const CONTAINER_PADDING_TOP = 0;
const PADDING_TOP = CONTAINER_PADDING_TOP + moderateScale(22, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);
const CLEAR_BTN_WIDTH = moderateScale(32, 0.3);

const FloatingLabelInput = forwardRef((props, ref) => {
  const {
    children,
    inputPaddingTop,
    inputPaddingLeft,
    noBorder,
    error,
    onFocus,
    onBlur,
    label,
    defaultValue,
    onChangeText,
    clearStyle,
    noClear,
    containerStyle,
    style,
    placeholderTextColor,
    ...allProps
  } = props;

  const containerPaddingTop = inputPaddingTop + (Metrics.isAndroid ? 4 : 0);
  const formPaddingTop = PADDING_TOP; // + inputPaddingTop

  const [isFocused, setIsFocused] = useState(defaultValue ? true : false);
  const [value, setValue] = useState(defaultValue);
  const animatedIsFocused = useRef(new Animated.Value(defaultValue ? 1 : 0))
    .current;

  useEffect(() => {
    Animated.timing(animatedIsFocused, {
      useNativeDriver: false,
      toValue: isFocused || value ? 1 : 0,
      duration: 200,
    }).start();
  }, [isFocused, value]);

  handleFocus = () => {
    setIsFocused(true);
    onFocus();
  };
  handleBlur = () => {
    setIsFocused(false);
    onBlur();
  };

  const onChangeTextFn = (text) => {
    setValue(text);
    onChangeText(text);
  };

  const onClear = () => {
    setValue('');
    onChangeText('');
  };

  return (
    <View
      style={[
        styles.inputView,
        {
          paddingTop: containerPaddingTop,
        },
        containerStyle,
      ]}>
      <View>
        <TextInput
          onBlur={handleBlur}
          onFocus={handleFocus}
          ref={ref}
          value={value}
          {...allProps}
          blurOnSubmit
          onChangeText={onChangeTextFn}
          style={[
            styles.inputContainer,
            {
              paddingTop: inputPaddingTop,
              paddingLeft: inputPaddingLeft,
              paddingRight: CLEAR_BTN_WIDTH,
            },
            !noBorder && {
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            },
            style,
          ]}
          underlineColorAndroid="transparent"
        />
        {!noClear && !!value && (
          <TouchableOpacity
            onPress={onClear}
            style={[
              {
                position: 'absolute',
                top: 0,
                bottom: 0,
                right: 0,
                width: CLEAR_BTN_WIDTH,
                justifyContent: 'center',
                alignItems: 'center',
              },
              clearStyle,
            ]}>
            <View
              style={{
                width: moderateScale(16, 0.3),
                height: moderateScale(16, 0.3),
                borderRadius: moderateScale(16, 0.3),
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.lightGray,
              }}>
              <SvgIcon
                fill={Colors.white}
                height={moderateScale(8, 0.3)}
                name="Close"
                width={moderateScale(8, 0.3)}
              />
            </View>
          </TouchableOpacity>
        )}
      </View>
      <Animated.View
        pointerEvents="none"
        style={{
          position: 'absolute',
          left: inputPaddingLeft,
          // top: animatedIsFocused.interpolate({
          //   inputRange: [0, 1],
          //   outputRange: [formPaddingTop, inputPaddingTop],
          // }),
          transform: [
            {
              translateY: animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [formPaddingTop, inputPaddingTop],
              }),
            },
          ],
        }}>
        <Animated.Text
          style={{
            fontSize: animatedIsFocused.interpolate({
              inputRange: [0, 1],
              outputRange: [FONT_SIZE, moderateScale(10, 0.3)],
            }),
            color: animatedIsFocused.interpolate({
              inputRange: [0, 1],
              outputRange: [placeholderTextColor, Colors.darkerGray],
            }),
          }}>
          {label}
        </Animated.Text>
      </Animated.View>
      <ErrorMessage
        style={{
          marginLeft: inputPaddingLeft,
        }}>
        {error}
      </ErrorMessage>
      {children}
    </View>
  );
});

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.white,
    color: Colors.text,
    fontSize: FONT_SIZE,
    height: INPUT_HEIGHT,
    width: '100%',
  },
  inputView: {
    backgroundColor: Colors.white,
  },
});

FloatingLabelInput.defaultProps = {
  defaultValue: '',
  inputPaddingTop: 0,
  inputPaddingLeft: 0, //FORM_PADDING,
  autoCapitalize: 'none',
  autoCompleteType: 'off',
  autoCorrect: false,
  keyboardType: Metrics.isIos ? 'ascii-capable' : 'default',
  returnKeyType: 'done',
  noBorder: false,
  placeholderTextColor: Colors.lightGray,
  clearStyle: {},
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
};

export default FloatingLabelInput;
