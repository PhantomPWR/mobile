import React from 'react';
import {StyleSheet} from 'react-native';
import {Colors, ApplicationStyles} from '../../Themes';
import {Label} from '../../Components/Text';
import {moderateScale} from 'react-native-size-matters/extend';

const {height} = ApplicationStyles.formRow;
export const BUTTON_HEIGHT = moderateScale(height, 0.3);

const Button = (props) => {
  const {
    children,
    style,
    disabled,
    label,
    labelStyle,
    selectedItem,
    placeholder,
    placeholderTextColor,
    ...allProps
  } = props;

  return (
    <>
      {!!label && <Label style={labelStyle}>{label}</Label>}
      <TouchableHighlight
        disabled={disabled}
        style={[
          styles.buttonContainer,
          style,
          disabled && {
            opacity: 0.7,
          },
        ]}
        {...allProps}>
        <>
          {!!placeholder && (
            <Label
              style={[styles.placeholder, {color: props.placeholderTextColor}]}>
              {placeholder}
            </Label>
          )}

          {children}
        </>
      </TouchableHighlight>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.white,
    borderColor: Colors.border,
    borderRadius: 8,
    borderWidth: 1,
    height: BUTTON_HEIGHT,
    justifyContent: 'center',
    marginBottom: moderateScale(16, 0.3),
    paddingLeft: 18,
    width: '100%',
  },
  placeholder: {
    fontSize: moderateScale(14, 0.3),
  },
});

Button.defaultProps = {
  labelStyle: {
    marginBottom: moderateScale(6),
  },
  placeholderTextColor: Colors.lightGray,
  underlayColor: Colors.silver,
  disabled: false,
};

export default Button;
