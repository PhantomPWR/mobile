import React, {useState, useRef} from 'react';
import {LayoutAnimation, Animated} from 'react-native';

import {View} from '../../../Components/Layout';

import PanelHeader from './PanelHeader';

const Panel = (props) => {
  const {
    renderHeader,
    isAccordion,
    isDefaultVisible,
    children,
    ...allProps
  } = props;
  const [isVisible, setIsVisible] = useState(!!isDefaultVisible);
  const animatedIsVisible = useRef(new Animated.Value(isVisible ? 1 : 0))
    .current;

  const onPress = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIsVisible((prevState) => {
      const newValue = !prevState;

      if (isAccordion) {
        Animated.timing(animatedIsVisible, {
          useNativeDriver: false,
          toValue: newValue ? 1 : 0,
          duration: 200,
        }).start();
      }

      return newValue;
    });
  };

  return (
    <>
      {!renderHeader ? (
        <PanelHeader
          animatedIsVisible={animatedIsVisible}
          isAccordion={isAccordion}
          onPress={onPress}
          {...allProps}
        />
      ) : (
        renderHeader(onPress, animatedIsVisible)
      )}

      <View
        style={[
          {
            overflow: 'hidden',
          },
          !isVisible && {
            height: 0,
          },
        ]}>
        {children}
      </View>
    </>
  );
};

export default Panel;
