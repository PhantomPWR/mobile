import React, {forwardRef, useState, useImperativeHandle} from 'react';
import {Switch} from 'react-native';
import {Colors} from '../../Themes';

let SwitchContainer = (props, ref) => {
  const {defaultValue, onValueChange, ...allProps} = props;
  const [isEnabled, setIsEnabled] = useState(defaultValue);

  const toggleSwitch = () => {
    setIsEnabled((previousState) => {
      const newValue = !previousState;
      onValueChange(newValue);
      return newValue;
    });
  };

  useImperativeHandle(
    ref,
    () => {
      return {
        toggleSwitch,
      };
    },
    [],
  );

  return (
    <Switch
      onValueChange={toggleSwitch}
      thumbColor={Colors.white}
      trackColor={{false: Colors.lightGray, true: Colors.success}}
      value={isEnabled}
      {...allProps}
    />
  );
};

SwitchContainer = forwardRef(SwitchContainer);

SwitchContainer.defaultProps = {
  defaultValue: false,
  onValueChange: () => {},
};

export default SwitchContainer;
