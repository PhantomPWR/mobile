import React, {useState, useCallback, useRef, useEffect} from 'react';
import {Platform, Animated, StyleSheet, TouchableOpacity} from 'react-native';
import {View, InfiniteScrollList} from '../../Layout';
import {Colors, ApplicationStyles} from '../../../Themes';
import {Label, ErrorMessage} from '../../Text';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../../../Components/SvgIcon';
import Item from './Item';
import Loader from '../../../Components/Loader';

const {height} = ApplicationStyles.formRow;
const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);
const CONTAINER_PADDING_TOP = 0;
const PADDING_TOP = CONTAINER_PADDING_TOP + moderateScale(22, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);

const FloatingLabelInfiniteSelect = (props) => {
  const {
    listTop,
    zIndex,
    inputPaddingTop,
    inputPaddingLeft,
    style,
    noBorder,
    error,
    children,
    onPressPicker,
    defaultValue,
    items,
    onSelectPickerItem,
    containerStyle,
    placeholder,
    placeholderTextColor,
    label,
    icon,
    iconSize,
    listProps,
    initialLoaded,
    ...allProps
  } = props;

  const containerPaddingTop = inputPaddingTop;
  const formPaddingTop = PADDING_TOP; // + inputPaddingTop

  const animatedHasSelected = useRef(new Animated.Value(defaultValue ? 1 : 0))
    .current;
  const dropdownRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState(defaultValue);
  const onPressPickerFn = useCallback(() => {
    setIsVisible(!isVisible);

    onPressPicker(!isVisible);
  }, [isVisible]);

  const onSelectPickerItemFn = (item) => {
    setSelectedItem(item);
    setIsVisible(false);

    onSelectPickerItem(item);
  };

  useEffect(() => {
    if (isVisible) {
      dropdownRef.current.resetPage();
    }
  }, [isVisible]);

  useEffect(() => {
    Animated.timing(animatedHasSelected, {
      useNativeDriver: false,
      toValue: selectedItem ? 1 : 0,
      duration: 100,
    }).start();
  }, [selectedItem]);

  return (
    <View
      style={[
        {
          ...Platform.select({
            ios: {
              zIndex, // zIndex for iOS only
            },
          }),
        },
        {
          paddingTop: containerPaddingTop,
        },
        containerStyle,
      ]}>
      <TouchableOpacity
        onPress={onPressPickerFn}
        {...allProps}
        style={[
          styles.selectContainer,
          {
            paddingTop: inputPaddingTop,
            paddingLeft: inputPaddingLeft,
          },
          !noBorder && {borderBottomWidth: 1, borderBottomColor: Colors.border},
          style,
        ]}>
        <>
          <Label
            style={{
              fontSize: FONT_SIZE,
              marginRight: moderateScale(4, 0.3),
            }}>
            {selectedItem}
          </Label>
          <View
            alignCenter
            style={{
              width: moderateScale(32, 0.3),
            }}>
            {!initialLoaded ? (
              <Loader color={Colors.gray} size="small" />
            ) : (
              <SvgIcon
                fill={Colors.black}
                height={iconSize}
                name={icon}
                width={iconSize}
              />
            )}
          </View>
        </>
      </TouchableOpacity>

      <Animated.Text
        pointerEvents="none"
        style={{
          position: 'absolute',
          left: inputPaddingLeft,
          top: animatedHasSelected.interpolate({
            inputRange: [0, 1],
            outputRange: [formPaddingTop, inputPaddingTop],
          }),
          fontSize: animatedHasSelected.interpolate({
            inputRange: [0, 1],
            outputRange: [FONT_SIZE, moderateScale(10, 0.3)],
          }),
          color: animatedHasSelected.interpolate({
            inputRange: [0, 1],
            outputRange: [placeholderTextColor, Colors.darkerGray],
          }),
        }}>
        {label}
      </Animated.Text>

      <ErrorMessage
        style={{
          marginLeft: inputPaddingLeft,
        }}>
        {error}
      </ErrorMessage>
      {children}

      <View
        style={[
          {
            backgroundColor: Colors.white,
            position: 'absolute',
            borderRadius: 4,
            top: listTop + containerPaddingTop,
            left: 0,
            width: '100%',
            maxHeight: PICKER_ITEM_HEIGHT * 5,
            zIndex, // zIndex for Android only

            elevation: 1,
            shadowColor: Colors.black,
            shadowRadius: 6,
            shadowOpacity: 0.2,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          },
          !isVisible && {
            position: 'relative',
            display: 'none',
          },
        ]}>
        <InfiniteScrollList
          getItemLayout={(data, index) => ({
            length: PICKER_ITEM_HEIGHT,
            offset: PICKER_ITEM_HEIGHT * index,
            index,
          })}
          keyboardShouldPersistTaps="always"
          // refreshControl={null}
          // contentContainerStyle={{
          //   paddingVertical: moderateScale(8, 0.3)
          // }}
          keyExtractor={(item, index) => item.id || String(index)}
          ref={dropdownRef}
          renderItem={({item}) => {
            const isSelected = item === selectedItem;
            return (
              <Item
                isSelected={isSelected}
                item={item}
                // disabled={isSelected}
                key={item}
                onPressItem={onSelectPickerItemFn}
              />
            );
          }}
          {...listProps}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  selectContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: PICKER_ITEM_HEIGHT,
    justifyContent: 'center',
  },
});

FloatingLabelInfiniteSelect.defaultProps = {
  items: [],
  inputPaddingTop: 0,
  inputPaddingLeft: 0, //FORM_PADDING,
  noBorder: false,
  onPressPicker: () => {},
  onSelectPickerItem: () => {},
  zIndex: 1000,
  defaultValue: '',
  listTop: PICKER_ITEM_HEIGHT,
  placeholderTextColor: Colors.lightGray,
  icon: 'DownChevron',
  iconSize: moderateScale(12, 0.3),
  listProps: {},
};

export default FloatingLabelInfiniteSelect;
