import React, {useState, useRef} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {View} from '../../../Components/Layout';
import {Label, ErrorMessage} from '../../../Components/Text';
import SvgIcon from '../../SvgIcon';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, ApplicationStyles, Metrics} from '../../../Themes';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

const {height} = ApplicationStyles.formRow;
const INPUT_HEIGHT = moderateScale(height, 0.3);
const FONT_SIZE = moderateScale(ApplicationStyles.fontSizes.slarge, 0.3);
const FORM_PADDING = moderateScale(14, 0.3);

const formatSelectedDate = (date) => {
  return date ? moment(date).format('YYYY-MM-DD h:mmA') : null;
};

const getFullDate = (date, time) => {
  const timeObj = moment(time);
  return moment(date)
    .set('hour', timeObj.get('hour'))
    .set('minute', timeObj.get('minute'))
    .toDate();
};

const DatePicker = (props) => {
  const {
    label,
    children,
    inputPaddingTop,
    inputPaddingLeft,
    containerStyle,
    style,
    onDateSelected,
    onPressPicker,
    defaultDate,
    placeholderTextColor,
    noMargin,
    noBorder,
    error,
    icon,
    iconSize,
    ...allProps
  } = props;

  const tempDateRef = useRef(moment());
  const [selectedDate, setSelectedDate] = useState(
    moment(defaultDate).toDate(),
  );
  const [displayedDate, setDisplayedDate] = useState(
    formatSelectedDate(defaultDate),
  );
  const [isVisibleDatePicker, setIsVisibleDatePicker] = useState(false);
  const [isVisibleTimePicker, setIsVisibleTimePicker] = useState(false);

  const containerPaddingTop = inputPaddingTop;

  const selectDate = (date) => {
    const formattedSelectedDate = formatSelectedDate(date);

    console.tron.log('formattedSelectedDate: ' + formattedSelectedDate);

    setSelectedDate(date);
    setDisplayedDate(formattedSelectedDate);
    onDateSelected(formattedSelectedDate);
  };

  const onDateSelectedFn = (date) => {
    setIsVisibleDatePicker(false);

    if (Metrics.isIos) {
      setTimeout(() => setIsVisibleTimePicker(true), 351);

      tempDateRef.current = date;
    } else {
      selectDate(date);
    }
  };

  const onTimeSelectedFn = (time) => {
    setIsVisibleTimePicker(false);

    console.tron.log('time', time);

    const date = getFullDate(tempDateRef.current, time);

    selectDate(date);
  };

  const onPressPickerFn = () => {
    setIsVisibleDatePicker(true);

    onPressPicker();
  };

  const onCancelPicker = () => {
    setIsVisibleDatePicker(false);
    setIsVisibleTimePicker(false);
  };

  return (
    <>
      <View
        style={[
          styles.inputView,
          {
            paddingTop: containerPaddingTop,
          },
          containerStyle,
        ]}>
        <TouchableOpacity
          onPress={onPressPickerFn}
          style={[
            styles.dateContainer,
            !noBorder && {
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            },
            style,
          ]}
          {...allProps}>
          <>
            {children}
            <Label
              style={{
                fontSize: FONT_SIZE,
                color: displayedDate ? Colors.text : placeholderTextColor,
              }}>
              {displayedDate || label}
            </Label>
            <View
              alignCenter
              style={{
                width: moderateScale(32, 0.3),
              }}>
              <SvgIcon
                fill={Colors.black}
                height={iconSize}
                name={icon}
                width={iconSize}
              />
            </View>
          </>
        </TouchableOpacity>
        <ErrorMessage
          style={{
            marginLeft: inputPaddingLeft,
          }}>
          {error}
        </ErrorMessage>
      </View>

      <DateTimePickerModal
        date={selectedDate}
        isVisible={isVisibleDatePicker}
        mode={Metrics.isIos ? 'date' : 'datetime'}
        onCancel={onCancelPicker}
        onConfirm={onDateSelectedFn}
      />

      {Metrics.isIos && (
        <DateTimePickerModal
          date={selectedDate}
          headerTextIOS={'Select Time'}
          isVisible={isVisibleTimePicker}
          mode="time"
          onCancel={onCancelPicker}
          onConfirm={onTimeSelectedFn}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  dateContainer: {
    alignItems: 'center',
    backgroundColor: Colors.white,
    color: Colors.text,
    flexDirection: 'row',
    height: INPUT_HEIGHT,
    justifyContent: 'space-between',
    width: '100%',
  },
  inputView: {
    backgroundColor: Colors.white,
  },
});

DatePicker.defaultProps = {
  inputPaddingTop: 0,
  inputPaddingLeft: 0, //FORM_PADDING,
  onDateSelected: () => {},
  onPressPicker: () => {},
  containerStyle: {},
  placeholderTextColor: Colors.lightGray,
  noBorder: false,
  noMargin: false,
  icon: 'Calendar',
  iconSize: moderateScale(12, 0.3),
};

export default DatePicker;
