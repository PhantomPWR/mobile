import React, {useState, useCallback, useRef, useEffect} from 'react';
import {ScrollView, Platform, StyleSheet, TouchableOpacity} from 'react-native';
import {View} from '../../Layout';
import {Colors, ApplicationStyles} from '../../../Themes';
import {Label} from '../../Text';
import {moderateScale} from 'react-native-size-matters/extend';
import SvgIcon from '../../../Components/SvgIcon';
import Item from './Item';

const {height} = ApplicationStyles.formRow;
export const PICKER_ITEM_HEIGHT = moderateScale(height, 0.3);

const Select = (props) => {
  const dropdownRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const {
    listTop,
    zIndex,
    children,
    onPressPicker,
    defaultValue,
    items,
    onSelectPickerItem,
    containerStyle,
    placeholder,
    placeholderTextColor,
    labelPrefix,
    style,
    ...allProps
  } = props;
  const [selectedItem, setSelectedItem] = useState(
    defaultValue || (items.length ? items[0] : ''),
  );
  const onPressPickerFn = useCallback(() => {
    setIsVisible(!isVisible);

    onPressPicker();
  }, [isVisible]);

  const onSelectPickerItemFn = (item) => {
    setIsVisible(false);

    if (item !== selectedItem) {
      setSelectedItem(item);
      onSelectPickerItem(item);
    }
  };
  useEffect(() => {
    if (dropdownRef.current) {
      const selectedItemIndex = items.indexOf(selectedItem);
      dropdownRef.current.scrollTo({
        y: selectedItemIndex * PICKER_ITEM_HEIGHT,
        animated: false,
      });
    }
  }, [isVisible]);

  return (
    <View
      style={[
        {
          ...Platform.select({
            ios: {
              zIndex, // zIndex for iOS only
            },
          }),
        },
        containerStyle,
      ]}>
      <TouchableOpacity
        onPress={onPressPickerFn}
        style={[styles.selectContainer, style]}
        {...allProps}>
        <>
          {children}
          <Label
            color="primary"
            style={{
              marginRight: moderateScale(4, 0.3),
            }}>
            {labelPrefix}
            {selectedItem}
          </Label>
          <SvgIcon
            fill={Colors.primary}
            height={moderateScale(12, 0.3)}
            name="DownChevron"
            width={moderateScale(12, 0.3)}
          />
        </>
      </TouchableOpacity>

      <View
        style={[
          {
            backgroundColor: 'white',
            position: 'absolute',
            borderRadius: 4,
            top: listTop,
            left: 0,
            width: '100%',
            maxHeight: PICKER_ITEM_HEIGHT * 5,
            zIndex, // zIndex for Android only

            elevation: 1,
            shadowColor: Colors.black,
            shadowRadius: 6,
            shadowOpacity: 0.2,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          },
          !isVisible && {
            position: 'relative',
            display: 'none',
          },
        ]}>
        <ScrollView keyboardShouldPersistTaps="always" ref={dropdownRef}>
          {items.map((item) => {
            const isSelected = item === selectedItem;
            return (
              <Item
                isSelected={isSelected}
                item={item}
                // disabled={isSelected}
                key={item}
                onPressItem={onSelectPickerItemFn}
              />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  selectContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: '100%',
    justifyContent: 'center',
  },
});

Select.defaultProps = {
  items: [],
  onPressPicker: () => {},
  onSelectPickerItem: () => {},
  zIndex: 1000,
  selectedItem: '',
  defaultValue: '',
  labelPrefix: '',
  listTop: '100%',
};

export default Select;
