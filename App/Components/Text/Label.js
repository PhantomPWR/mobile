import React from 'react';
import Text from './Normal';

// fontSize: ${props => moderateScale(!props.small ? 14 : 12, 0.3)}px;
const Label = (props) => {
  return <Text small {...props} />;
};

export default Label;
