import React from 'react';
import {StyleSheet} from 'react-native';
import Text from './Normal';
import {moderateScale} from 'react-native-size-matters/extend';

const Small = (props) => {
  const {style, ...allProps} = props;

  return <Text style={[styles.smallContainer, style]} {...allProps} />;
};

const styles = StyleSheet.create({
  smallContainer: {
    fontSize: moderateScale(10),
  },
});

export default Small;
