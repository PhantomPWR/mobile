import React from 'react';
import {StyleSheet} from 'react-native';
import Text from './Normal';
import {moderateScale} from 'react-native-size-matters/extend';

const SubHeading = (props) => {
  const {style, ...allProps} = props;

  return <Text style={[styles.subHeadingContainer, style]} {...allProps} />;
};

const styles = StyleSheet.create({
  subHeadingContainer: {
    fontSize: moderateScale(18),
    marginBottom: moderateScale(26),
  },
});

export default SubHeading;
