import React from 'react';
import {StyleSheet} from 'react-native';
import Text from './Normal';
import {Colors} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

const Error = (props) => {
  const {style, children, ...allProps} = props;

  if (!children) {
    return null;
  }

  return (
    <Text error style={[styles.errorContainer, style]} {...allProps}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  errorContainer: {
    color: Colors.error,
    fontSize: moderateScale(12, 0.2),
    marginTop: moderateScale(4, 0.2),
  },
});

export default Error;
