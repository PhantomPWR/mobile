import React from 'react';
import {Text} from 'react-native';
import {Colors, ApplicationStyles} from '../../Themes';
import {moderateScale} from 'react-native-size-matters/extend';

export const getSize = (props) => {
  const fontSizes = ApplicationStyles.fontSizes;

  if (props.xxxsmall) {
    return fontSizes.xxxsmall;
  } else if (props.xxsmall) {
    return fontSizes.xxsmall;
  } else if (props.xsmall) {
    return fontSizes.xsmall;
  } else if (props.small) {
    return fontSizes.small;
  } else if (props.slarge) {
    return fontSizes.slarge;
  } else if (props.large) {
    return fontSizes.large;
  } else if (props.xlarge) {
    return fontSizes.xlarge;
  } else if (props.xxlarge) {
    return fontSizes.xxlarge;
  } else if (props.xxxlarge) {
    return fontSizes.xxxlarge;
  } else if (props.xxxxlarge) {
    return fontSizes.xxxxlarge;
  } else {
    return fontSizes.medium;
  }
};

// use HEX Colors because it is faster than RGBA and Opacity
// https://stackoverflow.com/questions/38523826/text-css-rendering-performance-rgba-vs-hex-vs-opacity
// export const getTextColor = (props) => {
//   const color = COLOR_MATRIX[props.color][]
//   let color = !isGray ? Colors.text : Colors.darkerGray

//   if (isLight) {
//     color = !isGray ? Colors.lightText : Colors.lightGray
//   }

//   switch (weight) {
//     case 'medium':
//       return 'HelveticaNeue-Medium'
//     case 'bold':
//       return 'HelveticaNeue-Bold'
//     default:
//       return 'HelveticaNeue'
//   }

//   return color
// }

const TextWithFont = (props) => {
  const {
    center,
    children,
    error,
    weight,
    color,
    underline,
    style,
    ...allProps
  } = props;

  return (
    <Text
      style={[
        {
          fontFamily: ApplicationStyles.getFontByWeight(weight),
          color: Colors[color],
          fontSize: moderateScale(getSize(props), 0.3),
        },
        style,
        center && {textAlign: 'center'},
        error && {color: Colors.error},
        underline && {textDecorationLine: 'underline'},
      ]}
      {...allProps}>
      {children}
    </Text>
  );
};

TextWithFont.defaultProps = {
  style: {},
  color: 'text',
};

export default TextWithFont;
