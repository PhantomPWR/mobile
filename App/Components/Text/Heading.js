import React from 'react';
import {StyleSheet} from 'react-native';
import Text from './Normal';
import {moderateScale} from 'react-native-size-matters/extend';

const Heading = (props) => {
  const {style, ...allProps} = props;

  return (
    <Text
      style={[styles.headingContainer, style]}
      weight="semibold"
      {...allProps}
    />
  );
};

const styles = StyleSheet.create({
  headingContainer: {
    fontSize: moderateScale(26, 0.3),
  },
});

export default Heading;
