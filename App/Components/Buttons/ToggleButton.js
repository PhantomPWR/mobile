import React, {useState} from 'react';
import IconButton from './IconButton';

const ToggleButton = (props) => {
  const {
    onPress,
    iconToggled,
    iconUntoggled,
    iconSize,
    iconFill,
    defaultValue,
    ...allProps
  } = props;
  const [isToggled, setIsToggled] = useState(defaultValue);

  const onPressFn = () => {
    setIsToggled((prevState) => {
      const newValue = !prevState;

      onPress(newValue);

      return newValue;
    });
  };

  return (
    <IconButton
      icon={isToggled ? iconToggled : iconUntoggled}
      iconFill={iconFill}
      iconSize={iconSize}
      onPress={onPressFn}
      {...allProps}
    />
  );
};

ToggleButton.defaultProps = {
  defaultValue: false,
  onPress: () => {},
};

export default ToggleButton;
