import React from 'react';
import {TouchableHighlight, StyleSheet} from 'react-native';
import {Colors} from '../../Themes';
import {View} from '../../Components/Layout';
import {Text} from '../../Components/Text';
import Loader from '../../Components/Loader';
import {moderateScale} from 'react-native-size-matters/extend';

const Button = (props) => {
  const {
    isLoading,
    style,
    disabled,
    label,
    labelStyle,
    icon,
    ...allProps
  } = props;

  return (
    <TouchableHighlight
      disabled={isLoading || disabled}
      style={[
        styles.buttonContainer,
        {backgroundColor: Colors.primary},
        style,
        disabled && {
          opacity: 0.5,
        },
      ]}
      underlayColor={Colors.secondary}
      {...allProps}>
      <>
        {!isLoading && !!label && (
          <View alignCenter row>
            <Text
              style={[
                {
                  color: Colors.white,
                  fontSize: moderateScale(18, 0.3),
                },
                labelStyle,
              ]}
              weight="bold">
              {label}
            </Text>
            {icon}
          </View>
        )}
        {isLoading && <Loader color={Colors.white} />}
      </>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
    borderRadius: 2,
    height: moderateScale(44, 0.3),
    justifyContent: 'center',
    width: '100%',
  },
});

Button.defaultProps = {
  disabled: false,
};

export default Button;
