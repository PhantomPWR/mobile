import React from 'react';
import {Animated, TouchableOpacity, StyleSheet} from 'react-native';
import SvgIcon from '../../Components/SvgIcon';
import {View} from '../../Components/Layout';
import {moderateScale} from 'react-native-size-matters/extend';
import {Colors, Metrics} from '../../Themes';
import {is} from 'ramda';

const IconButton = (props) => {
  const {
    containerStyle,
    isAnimated,
    onPress,
    iconSize,
    icon,
    iconFill,
    ...allProps
  } = props;

  const IconView = isAnimated ? Animated.View : View;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.iconButtonContainer,
        {
          paddingHorizontal:
            props.style && is(Number, props.style.paddingHorizontal)
              ? props.style.paddingHorizontal
              : Metrics.spaceHorizontal,
        },
        containerStyle,
      ]}>
      <IconView {...allProps}>
        <SvgIcon
          fill={iconFill}
          height={iconSize}
          name={icon}
          width={iconSize}
        />
      </IconView>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  iconButtonContainer: {
    height: moderateScale(44, 0.4),
    justifyContent: 'center',
  },
});

IconButton.defaultProps = {
  iconSize: 18,
  iconFill: Colors.white,
  isAnimated: false,
};

export default IconButton;
