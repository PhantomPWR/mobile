import makeVar from './makeVar';

export const setupDetailsVar = makeVar(
  {appName: '', apiKey: ''},
  'setupDetails',
);
export const userDetailsVar = makeVar({email: '', password: ''}, 'userDetails');
export const sessionIdVar = makeVar({sessionId: ''}, 'sessionId');
export const noteFiltersVar = makeVar(
  {
    filterBy: 'terms',
    terms: '',
    tags: [],
  },
  'noteFilters',
);
export const taskFiltersVar = makeVar(
  {
    filterBy: 'terms',
    terms: '',
    tags: [],
  },
  'taskFilters',
);

export {makeVar};
