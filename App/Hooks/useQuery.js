import {useState, useEffect, useCallback, useRef} from 'react';
import {Alert} from 'react-native';
import {callGraphqlQuery} from '../GraphQL';
import SentryConfig from '../Config/SentryConfig';
import Const from '../Constants';
import NetInfo from '@react-native-community/netinfo';
import {readGraphqlCache} from '.';

const hasConnection = async () => {
  const state = await NetInfo.fetch();
  console.tron.log('Is connected?', state.isConnected);
  return state.isConnected;
};

const showNoConnectionAlert = () => {
  Alert.alert(
    'No Connection',
    'This action needs network connection in order to proceed',
    [{text: 'OK'}],
  );
};

const initialOptions = {
  skipInitialize: false,
  merge: false,
  fetchPolicy: Const.FETCH_POLICIES.NETWORK_ONLY,
};

export default function useQuery(graphqlQuery, variables, options) {
  const isMounted = useRef(false);
  const baseOptions = {...initialOptions, ...options};

  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  });

  const setStateFn = useCallback((callback, value) => {
    if (isMounted.current) {
      callback(value);
    }
  }, []);

  const [loading, setLoading] = useState(true);
  const [initialLoaded, setInitialLoaded] = useState(false);
  const [error, setError] = useState('');
  const [data, setData] = useState({});

  // console.tron.log("useQuery rerender: " + graphqlQuery)

  const callQuery = useCallback(async (variables2, mutationKey, options2) => {
    const finalOptions = {...baseOptions, ...options2};

    setStateFn(setLoading, true);
    setStateFn(setError, '');

    if (mutationKey) {
      const isConnected = await hasConnection();
      if (!isConnected) {
        showNoConnectionAlert();
        setStateFn(setLoading, false);
        return;
      }
    }

    let finalVariables = variables;

    if (variables2) {
      finalVariables = variables2;
    }

    if (finalOptions.directCache) {
      const cache = readGraphqlCache(graphqlQuery, variables2);
      setStateFn(setData, cache);
    } else if (typeof graphqlQuery === 'string') {
      try {
        const {cacheFn} = finalVariables;
        await cacheFn(
          setStateFn.bind(null, setData),
          graphqlQuery,
          finalOptions,
        );
      } catch (err) {
        SentryConfig.captureException(err);
      }
    } else {
      try {
        const response = await callGraphqlQuery(
          graphqlQuery,
          finalVariables,
          mutationKey,
          finalOptions.fetchPolicy,
        );
        console.tron.log('graphqlQuery response', response);
        setStateFn(setData, response.data);
      } catch (err) {
        console.tron.log('graphqlQuery error', err);

        if (Array.isArray(err.graphQLErrors)) {
          const {message} = err.graphQLErrors[0];
          setStateFn(setError, message);
        } else {
          setStateFn(
            setError,
            "Something's wrong connecting with the server, please try again",
          );
        }

        SentryConfig.captureException(err);
      }
    }

    if (!initialLoaded) {
      setStateFn(setInitialLoaded, true);
    }

    setStateFn(setLoading, false);
  }, []);

  useEffect(() => {
    if (!baseOptions.skipInitialize) {
      const initialize = async () => {
        if (variables) {
          await callQuery();
        } else {
          setStateFn(setLoading, false);
        }

        setStateFn(setInitialLoaded, true);
      };

      initialize();
    }

    return () => {};
  }, []);

  return {loading, initialLoaded, error, data, callQuery};
}
