export default function useRectiveVar(makeVar) {
  return makeVar(null, null, {directCache: true});
}
